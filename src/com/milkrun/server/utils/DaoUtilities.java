package com.milkrun.server.utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.RetailStore;

public class DaoUtilities {

	public static String gaeSQLConnectionString = "jdbc:google:rdbms://piersmacdonald.com:milkrun:mrdv01/milkrundb01";
	public static String gaeSQLConnectionStringTEST = "jdbc:mysql://localhost:3306/milkrun?user=root&password=trilby&useInformationSchema=true&useUnicode=true&characterEncoding=UTF8&useServerPrepStmts=true";
	public static String queryXMLFilePath = "WEB-INF/Queries.xml";

	/**
	 * Work around because JDBC PreparedStatments don't support IN clauses
	 * 
	 * @param queryName name of the query in XML table
	 * @param inClause List on values to go in the IN clause 
	 * @return Query completed query
	 * @throws NoSuchQueryException
	 */
	public static String getINQuery(String queryName, String inClause) throws NoSuchQueryException{
		String query = getQueryFromXML(queryName).trim();
		return query.replaceAll("\\?", inClause);		
	}

	public static String getINQuery(String queryName, List<BasketItem> items){
		if(queryName.equals("getAllItems")){
			return "";
		}else if(queryName.equals("ShopBasketQueries.ShopUserBasket")){
			return 
			
			"SELECT " +	
			"    six.retail_store_id, "+
			"    rs.store_name_txt, "+
			"    six.basket_item_id, "+
			"    six.price_txt "+
			"FROM  "+
			"    store_item_xref six, "+
			"    retail_store rs "+
			"WHERE "+
			"    six.retail_store_id = rs.retail_store_id "+
			"AND     "+
			"    six.basket_item_id IN (" + getBasketItemInString(items) +")  "+
			"ORDER BY "+
			"    six.retail_store_id ";
		}
		
		return null;
	}
	
	public static String getBasketItemInString(List<BasketItem> items){
		String s = "";
		
		for(int i = 0; i < items.size(); i++){
			if(i == 0)
				s += items.get(i).basketItemID;
			else
				s += "," + items.get(i).basketItemID;
		}
		
		return s;
	}
	
	public static String generateInClause(List<Integer> ids){
		String s = "";
		
		for(int i = 0; i < ids.size(); i++){
			if(i == 0)
				s += ids.get(i);
			else
				s += "," + ids.get(i);
		}
		
		return s;
	}
	
	public static String getStoreInString(List<RetailStore> stores){
		String s = "";
		
		for(int i = 0; i < stores.size(); i++){
			if(i == 0)
				s += stores.get(i).retailStoreID;
			else
				s += "," + stores.get(i).retailStoreID;
		}
		
		return s;
	}
	
    public static String getQueryFromXML(String nameOfQuery) throws NoSuchQueryException
    {
    	String reportQuery = "";
    	NodeList nl = null;
		Document dom = null;
		try {
			

			File tBackendFile = new File(queryXMLFilePath);
			dom= DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(tBackendFile);

			
			//Using factory get an instance of document builder
			//DocumentBuilder db = dbf.newDocumentBuilder();

			//parse using builder to get DOM representation of the XML file
			//dom = db.parse(queryXMLFilePath);
			
			Element docEle = dom.getDocumentElement();
			nl = docEle.getElementsByTagName(nameOfQuery);
			
			if(nl != null && nl.getLength() > 0) {
				Element el = (Element)nl.item(0);
				reportQuery = el.getFirstChild().getNodeValue();
			}
			//todo: handle exceptions
		}catch(ParserConfigurationException pce) {
			pce.printStackTrace();
		}catch(SAXException se) {
			se.printStackTrace();
		}catch(IOException ioe) {
			ioe.printStackTrace();
		}
		
		if(!reportQuery.equals(""))
			return reportQuery.trim();
		else
			throw new NoSuchQueryException();
    }
}
