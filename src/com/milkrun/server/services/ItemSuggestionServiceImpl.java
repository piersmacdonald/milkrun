package com.milkrun.server.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

import com.google.appengine.api.rdbms.AppEngineDriver;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.milkrun.client.services.ItemSuggestionService;
import com.milkrun.server.dal.dao.ItemDAO;
import com.milkrun.server.utils.DaoUtilities;
import com.milkrun.shared.dto.ItemSuggestion;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class ItemSuggestionServiceImpl extends RemoteServiceServlet implements ItemSuggestionService {

	private static final Logger log = Logger.getLogger(ItemSuggestionServiceImpl.class.getName());
	
	@Override
	public ItemSuggestion suggestByString(String searchString) throws IllegalArgumentException {

   		log.info("MILKRUN - You hit the Get method of ItemSuggestion");
   		
   		ItemSuggestion suggestion = new ItemSuggestion();
   		   		
   		try
   		{
   			//suggestion.suggestedItems = new ArrayList<BasketItem>();
   			suggestion.inputString = searchString;
   			ItemDAO.INSTANCE.setConnection(getNewDBConnection());
   			suggestion.suggestedItems = ItemDAO.INSTANCE.suggestItems(searchString);
   		}
   		catch(Exception e)
   		{
   			log.severe("Failed to get list of suggestions from DB for string: '" + searchString + "' " + e.getMessage());
   		}
   		   	
   		return suggestion;
	}	
	
	private Connection getNewDBConnection(){
		Connection c = null;
   		try {
			DriverManager.registerDriver(new AppEngineDriver());
			c = DriverManager.getConnection(DaoUtilities.gaeSQLConnectionString);
			c.setAutoCommit(true);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return c;
	}
}
