package com.milkrun.server.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import com.google.appengine.api.rdbms.AppEngineDriver;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.milkrun.client.services.UserBasketService;
import com.milkrun.server.dal.dao.BasketDAO;
import com.milkrun.server.dal.dao.ShoppingDAO;
import com.milkrun.server.utils.DaoUtilities;
import com.milkrun.server.utils.PricingUtilities;
import com.milkrun.shared.dto.ShoppingResults;
import com.milkrun.shared.dto.UserBasket;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class UserBasketServiceImpl extends RemoteServiceServlet implements UserBasketService {

	private static final Logger log = Logger.getLogger(UserBasketServiceImpl.class.getName());

	@Override
	public String removeUserBasket(String basketID) {
		try{
			BasketDAO.INSTANCE.setConnection(getNewDBConnection());
			BasketDAO.INSTANCE.deleteUserBasket(basketID);
		}catch( Exception e ){
			// TODO Auto-generated catch block
		}
		return "";
	}

	@Override
	public UserBasket getUserBasket(String basketID) {
   		
    	log.info("About to get a users basket - " + basketID);
    	
    	BasketDAO.INSTANCE.setConnection(getNewDBConnection());
    	UserBasket result = BasketDAO.INSTANCE.getUserBasket(basketID);
    	
   		return result;
	}


	@Override
	public String updateUserBasket(UserBasket basket) {
    	log.info("Updating a basket");
    	

    	try{
    		BasketDAO.INSTANCE.setConnection(getNewDBConnection());
    		BasketDAO.INSTANCE.updateUserBasket(basket);
    	}catch(Exception e){
    		log.severe("Crashed updating a basket");
    		return "problem";
    	}
    	
    	
    	return new String("User Basket - " + basket.userBasketID + "updated");
	}

	@Override
	public List<UserBasket> listUserBaskets(String userID) {
    	log.info("About to list some a users baskets");
    	
    	BasketDAO.INSTANCE.setConnection(getNewDBConnection());
    	List<UserBasket> result = BasketDAO.INSTANCE.listUserBaskets(userID);
    	
   		return result;
	}


	@Override
	public String createUserBasket(UserBasket basket) {
    	log.info("Creating a basket");
    	int newBasketId = 0;
    	
    	try{
    		BasketDAO.INSTANCE.setConnection(getNewDBConnection());
    		newBasketId = BasketDAO.INSTANCE.createUserBasket(basket);
    	}catch(Exception e){
    		log.severe("Crashed creating a basket - " + e.getLocalizedMessage());
    	}
    	
    	
    	return new String(newBasketId + "");	 
	}

	@Override
	public ShoppingResults shopUserBasket(String basketID) {
    	log.info("shopping a basket");
    	ShoppingResults result = null;
    	
    	BasketDAO.INSTANCE.setConnection(getNewDBConnection());
    	UserBasket basket = BasketDAO.INSTANCE.getUserBasket(basketID);
    	
    	result = new ShoppingResults();
    	
    	ShoppingDAO.INSTANCE.setConnection(getNewDBConnection());
    	result.baskets = ShoppingDAO.INSTANCE.shopBasket(basket);
        
    	result = PricingUtilities.calculateBasketPrices(result);
    	
        return result;
	}

	private Connection getNewDBConnection(){
		Connection c = null;
   		try {
			DriverManager.registerDriver(new AppEngineDriver());
			c = DriverManager.getConnection(DaoUtilities.gaeSQLConnectionString);
			c.setAutoCommit(true);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return c;
	}

}
