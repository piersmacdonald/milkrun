package com.milkrun.server.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import com.google.appengine.api.rdbms.AppEngineDriver;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.milkrun.client.services.BasketItemService;
import com.milkrun.server.dal.dao.ItemDAO;
import com.milkrun.server.utils.DaoUtilities;

import com.milkrun.shared.dto.AppUser;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.ItemSuggestion;


/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class BasketItemServiceImpl extends RemoteServiceServlet implements BasketItemService {

	private static final Logger log = Logger.getLogger(BasketItemServiceImpl.class.getName());


	
	public ItemSuggestion suggestByString(String searchString) throws IllegalArgumentException {

   		log.info("MILKRUN - You hit the Get method of ItemSuggestion");
   		   		
   		ItemSuggestion suggestion = new ItemSuggestion();
   		
   		try
   		{
   			//suggestion.suggestedItems = new ArrayList<BasketItem>();
   			suggestion.inputString = searchString;
   			ItemDAO.INSTANCE.setConnection(getNewDBConnection());
   			suggestion.suggestedItems = ItemDAO.INSTANCE.suggestItems(searchString);
   		}
   		catch(Exception e)
   		{
   			log.severe("Failed to get list of suggestions from DB for string: '" + searchString + "' " + e.getMessage());
   		}
   		   	
   		return suggestion;
	}

	@Override
	public List<BasketItem> listBasketItems() {
    	log.info("About to list some items");
    	
    	ItemDAO.INSTANCE.setConnection(getNewDBConnection());
    	List<BasketItem> allItems = ItemDAO.INSTANCE.listBasketItems(10);
    	
   		return allItems;
	}

	@Override
	public String createBasketItem(BasketItem item) throws IllegalArgumentException {
		
		String result = null;
 	    	
    	try
    	{
    		ItemDAO.INSTANCE.setConnection(getNewDBConnection());
    		int newItemID = ItemDAO.INSTANCE.createBasketItem(item, new AppUser());
    		log.info("Just added new item: " + item.basketItemID + " " + item.description);
    		result = new String(newItemID +"");
    	}catch(Exception e){
    		log.severe("BasketItemServiceImpl.createBasketItem " + e.getLocalizedMessage());
    		result = new String("Error creating item");
    	}
    	
		return result;
	}

	@Override
	public String removeBasketItem(String basketItemID)
			throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BasketItem getBasketItem(String basketItemID) throws IllegalArgumentException {
    	
    	ItemDAO.INSTANCE.setConnection(getNewDBConnection());
    	BasketItem item = ItemDAO.INSTANCE.getBasketItem(Integer.parseInt(basketItemID));
    	
   		return item;
	}

	@Override
	public String updateBasketItem(BasketItem item)	throws IllegalArgumentException {
    	return "";
	}	
	

	public List<String> getUpcType() {
		List<String> result = null;
	    	
    	try
    	{
    		ItemDAO.INSTANCE.setConnection(getNewDBConnection());
    		result= ItemDAO.INSTANCE.getUpcTypes();
    	}catch(Exception e){
    		log.severe("BasketItemServiceImpl.getUpcType - " + e.getLocalizedMessage());
    	}
    	
		return result;
	}	
	
	private Connection getNewDBConnection(){
		Connection c = null;
   		try {
			DriverManager.registerDriver(new AppEngineDriver());
			c = DriverManager.getConnection(DaoUtilities.gaeSQLConnectionString);
			c.setAutoCommit(true);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return c;
	}

}
