package com.milkrun.server.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.google.appengine.api.rdbms.AppEngineDriver;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.milkrun.client.services.StoreService;
import com.milkrun.server.dal.dao.StoreDAO;
import com.milkrun.server.utils.DaoUtilities;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.RetailStore;

/**
 * The server side implementation of the RPC service.
 */
/**
 * @author Piers
 *
 */
@SuppressWarnings("serial")
public class StoreServiceImpl extends RemoteServiceServlet implements StoreService {

	private static final Logger log = Logger.getLogger(StoreServiceImpl.class.getName());

	private Connection getNewDBConnection(){
		Connection c = null;
   		try {
			DriverManager.registerDriver(new AppEngineDriver());
			c = DriverManager.getConnection(DaoUtilities.gaeSQLConnectionString);
			c.setAutoCommit(true);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return c;
	}
	
	public List<RetailStore> listRetailStores() {
    	
    	List<RetailStore> stores = null;
    	
		try {
			StoreDAO.INSTANCE.setConnection(getNewDBConnection());
			stores = StoreDAO.INSTANCE.listRetailStores();
		} catch (Exception e) {
    		log.severe("BasketItemServiceImpl.listRetailStores " + e.getLocalizedMessage());
		} finally{
			StoreDAO.INSTANCE.closeConnection();
		}
    	 
   		return stores;
	}

	public String createRetailStore(RetailStore store) throws IllegalArgumentException {
		
		String result = null;
	    	
    	try
    	{
    		StoreDAO.INSTANCE.setConnection(getNewDBConnection());
    		int newID = StoreDAO.INSTANCE.addRetailStore(store);
    		result = new String(newID +"");
    	}catch(Exception e){
    		log.severe("BasketItemServiceImpl.createBasketItem " + e.getLocalizedMessage());
    		result = new String("Error creating item");
    	} finally{
			StoreDAO.INSTANCE.closeConnection();
		}
    	
		return result;
	}

	public String removeRetailStore(String storeID) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

	public RetailStore getRetailStore(String storeID) throws IllegalArgumentException {
    	
		RetailStore result = null;
		
		try
    	{
			StoreDAO.INSTANCE.setConnection(getNewDBConnection());
			result = StoreDAO.INSTANCE.getRetailStore(Integer.parseInt(storeID));
    	}catch(Exception e){
    		log.severe("StoreServiceImpl.getRetailStore " + e.toString());
    		result = null;
    	} finally{
			StoreDAO.INSTANCE.closeConnection();
		}
    

    	
   		return result;
	}

	public String updateRetailStore(RetailStore store) throws IllegalArgumentException {
		
		String result = null;
    	
    	try
    	{
    		StoreDAO.INSTANCE.setConnection(getNewDBConnection());
    		StoreDAO.INSTANCE.updateRetailStore(store);
    		result = new String("1");
    	}catch(Exception e){
    		log.severe("StoreServiceImpl.updateRetailStore " + e.toString());
    		result = new String("0");
    	} finally{
			StoreDAO.INSTANCE.closeConnection();
		}
    	
		return result;
	}

	public List<RetailStore> suggestStoreByString(String searchString) throws IllegalArgumentException {
		
	
   		List<RetailStore> suggestion = new ArrayList<RetailStore>();
   		
   		try
   		{  
   			StoreDAO.INSTANCE.setConnection(getNewDBConnection());
   			suggestion = StoreDAO.INSTANCE.suggestStore(searchString);
   		}
   		catch(Exception e)
   		{
   			log.severe("Failed to get list of store suggestions from DB for string: '" + searchString + "' " + e.getMessage());
   		} finally{
			StoreDAO.INSTANCE.closeConnection();
		}
   		   	
   		return suggestion;
	}

	
	public String mapItemToStore(RetailStore store, BasketItem item) throws Exception {
		
		String result = null;
    	
    	try
    	{
    		StoreDAO.INSTANCE.setConnection(getNewDBConnection());
    		StoreDAO.INSTANCE.addPricedItemToStore(store, item);
    		result = new String("1");
    	}catch(Exception e){
    		log.severe("StoreServiceImpl.mapItemToStore " + e.getLocalizedMessage());
    		result = new String("Error creating item");
    	} finally{
			StoreDAO.INSTANCE.closeConnection();
		}
    	
		return result;
	}

	/**
	 * 
	 */
	public List<BasketItem> getPricedItems(RetailStore store) throws Exception {
		
		List<BasketItem> result = null;
		
		try{
	    	StoreDAO.INSTANCE.setConnection(getNewDBConnection());
	    	 result = StoreDAO.INSTANCE.getStoreInventory(store);
		}catch(Exception e){
			log.severe("StoreServiceImpl.getPricedItems " + e.getLocalizedMessage());
			throw new Exception(e.getMessage());
		} finally{
			StoreDAO.INSTANCE.closeConnection();
		}

    	
   		return result;
	}

}
