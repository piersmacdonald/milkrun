package com.milkrun.server.dal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.milkrun.server.dal.utilities.NamedParameterStatement;
import com.milkrun.server.utils.DaoUtilities;
import com.milkrun.server.utils.NoSuchQueryException;
import com.milkrun.shared.dto.AppUser;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.Brand;
import com.milkrun.shared.dto.Manufacturer;
import com.milkrun.shared.dto.UPI;


/**
 * @author Piers
 *
 */
public enum ItemDAO{
	INSTANCE;

	private static final Logger log = Logger.getLogger(ItemDAO.class.getName());
	private Connection connection = null;
	
	
	public void setConnection(Connection c){ connection = c; }
	
	public void closeConnection(){
		if (connection != null){ 
			try {
				connection.close();
	        }catch (SQLException ignore) {
	        }
		}
	}
	
	public List<BasketItem> listBasketItems(int maxrows) 
	{
		List<BasketItem> items = new ArrayList<BasketItem>();
		
		try {
			
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("BasketItemQueries.ListBasketItem"));
			ps.setInt("max_row", maxrows);
			
			ResultSet rs = ps.executeQuery();

			while (rs.next()){
				items.add(GetItemFromResultSet(rs));
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			log.severe("Error listing items: " + e.toString());
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
		
		return items;
	}

	/**
	 * @author Piers
	 * 
	 * This method allows for the creation of Basket Item in the database from a BasketItem DTO
	 * 
	 * @param item - BasketItem to be created in the database
	 * @param user - User creating the item
	 * @return
	 * @throws SQLException
	 * @throws NoSuchQueryException
	 */
	public int createBasketItem(BasketItem item, AppUser user) throws SQLException, NoSuchQueryException 
	{
		int key = -1;
		
		try {
			int upcID = getUPIIDByName(item.upc);
			if(upcID == -1){
				item.upc.upiID = insertUPI(item.upc, user);;
			}else{
				item.upc.upiID = upcID;
			}
			
			
			//todo: remove this
			int manuID = getManufacturerIDByName(item.brand.manufacturer);
			if(manuID == -1){
				item.brand.manufacturer.manufacturerID = insertManufacturer(item.brand.manufacturer, user);
			}else{
				item.brand.manufacturer.manufacturerID = manuID;
			}
			
			int brandID = getBrandIDByName(item.brand);
			if(brandID == -1){
				item.brand.brandID = insertBrand(item.brand, user);
			}else{
				item.brand.brandID = brandID;
			}
			
			key = insertBasketItem(item, user);
			//connection.commit();
		} catch (Exception e) {
			//connection.rollback();
			e.printStackTrace();
		}finally{
			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
		
		
		return key;
	}
	
	
	/**
	 * @param item
	 * @throws NoSuchQueryException 
	 */
	private int insertBasketItem(BasketItem item, AppUser user) throws NoSuchQueryException{
		
		int newKey = 0;

		try {
			
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("BasketItemQueries.InsertBasketItem"), Statement.RETURN_GENERATED_KEYS);
			
			ps.setString("short_desc_txt", item.name);
			ps.setString("detail_desc_txt", item.description);
			ps.setString("detail2_desc_txt", item.detailDescription);
			ps.setInt("brand_id", item.brand.brandID);
			ps.setString("quantity_num", item.quantity);
			ps.setString("quantity_unit_id", item.quantityType);
			ps.setString("upc_txt", item.upc.upiCode); //todo: fix this
			ps.setInt("create_user_id", user.userID);
			ps.setInt("last_updt_user_id", user.userID);
		    ps.executeUpdate();
		    		         
		    ResultSet rs = ps.getStatement().getGeneratedKeys();
		    if (rs != null && rs.next()) {
		    	newKey = rs.getInt(1);
		    } 

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			throw e;
		} finally {

		}
		
		return newKey;

	}
	
	private int insertUPI(UPI upc, AppUser user){
		
		int newKey = 0;

		try {
			
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("BasketItemQueries.InsertUPI"), Statement.RETURN_GENERATED_KEYS);
			
			ps.setString("upi_type_cd", upc.upiType);
			ps.setString("upi_code_txt", upc.upiCode);
			ps.setInt("create_user_id", user.userID);
			ps.setInt("last_updt_user_id", user.userID);		
		    ps.executeUpdate();
		         
		    ResultSet rs = ps.getStatement().getGeneratedKeys();
		    if (rs != null && rs.next()) {
		    	newKey = rs.getInt(1);
		    } 

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
/*			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}*/
		}
		
		return newKey;

	}
	
	private int insertBrand(Brand brand, AppUser user){
		int newKey = 0;

		try {
			
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("BasketItemQueries.InsertBrand"), Statement.RETURN_GENERATED_KEYS);
			
			ps.setString("brand_name", brand.brandName);
			ps.setString("description", brand.description);
			ps.setInt("company_id", brand.manufacturer.manufacturerID);
			ps.setInt("create_user_id", user.userID);
			ps.setInt("last_updt_user_id", user.userID);
		    ps.executeUpdate();
		    		         
		    ResultSet rs = ps.getStatement().getGeneratedKeys();
		    if (rs != null && rs.next()) {
		    	newKey = rs.getInt(1);
		    } 

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
/*			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}*/
		}
		
		return newKey;

	}
	
	private int insertManufacturer(Manufacturer manu, AppUser user){
		int newKey = 0;

		try {
			
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("BasketItemQueries.InsertManufacturer"), Statement.RETURN_GENERATED_KEYS);
			
			ps.setString("company_name", manu.companyName);
			ps.setInt("parent_company_id", manu.parentCompanyID);
			ps.setInt("primary_address_id", manu.primaryAddress);
			ps.setInt("secondary_address_id", manu.secondaryAddress);
			ps.setInt("create_user_id", user.userID);
			ps.setInt("last_updt_user_id", user.userID);

		    ps.executeUpdate();
		         
		    ResultSet rs = ps.getStatement().getGeneratedKeys();
		    if (rs != null && rs.next()) {
		    	newKey = rs.getInt(1);
		    } 

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
/*			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}*/
		}
		
		return newKey;
	}
	
	private int getBrandIDByName(Brand b) {

		int id = -1;

		try {
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("BasketItemQueries.GetBrandByName"));	
			ps.setString("brand_name", b.brandName);
			ps.setString("description", b.description);
			ResultSet rs = ps.executeQuery();
			
		    if (rs != null && rs.next()) {
		    	id = rs.getInt(1);
		    } 
		    		
		} catch (SQLException e) {
			log.severe("Error getting basket: " + e.toString());
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
/*			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}*/
		}
		
		return id;
	}
	
	private int getUPIIDByName(UPI u) {

		int id = -1;

		try {
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("BasketItemQueries.GetUPIByName"));	
			ps.setString("upi_type_cd", u.upiType);
			ps.setString("upi_code_txt", u.upiCode);
			ResultSet rs = ps.executeQuery();
			
		    if (rs != null && rs.next()) {
		    	id = rs.getInt(1);
		    } 
		    		
		} catch (SQLException e) {
			log.severe("getUPIIDByName: " + e.toString());
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
/*			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}*/
		}
		
		return id;
	}
	
	private int getManufacturerIDByName(Manufacturer m) throws Exception {

		int id = -1;

		try {
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("BasketItemQueries.GetManufacturerByName"));	
			ps.setString("company_name", m.companyName);
			ResultSet rs = ps.executeQuery();
			
		    if (rs != null && rs.next()) {
		    	id = rs.getInt(1);
		    } 
		    		
		} catch (SQLException e) {
			log.severe("Error getting basket: " + e.toString());
		} catch (NoSuchQueryException e) {
			throw e;
		} finally {

		}
		
		return id;
	}
	
	public BasketItem getBasketItem(int basketItemID) {

		BasketItem b = new BasketItem();

		try {

			PreparedStatement ps = connection.prepareStatement(DaoUtilities.getQueryFromXML("getBasketItemByID"));
			ps.setInt(1, basketItemID);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()){
				b = GetItemFromResultSet(rs);
			}
			
		} catch (SQLException e) {
			log.severe("Error getting basket: " + e.toString());
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
		
		return b;
	}
	
	public List<BasketItem> getBasketItems(List<Integer> ids) {

		List<BasketItem> items = new ArrayList<BasketItem>();

		try {
			String s = DaoUtilities.getINQuery("BasketItemQueries.GetBasketItemIN", DaoUtilities.generateInClause(ids));
			NamedParameterStatement ps = new NamedParameterStatement(connection, s);
			
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()){
				items.add(GetItemFromResultSet(rs));
			}
			
		} catch (SQLException e) {
			log.severe("Error getting basket: " + e.toString());
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return items;
	}

	public List<BasketItem> suggestItems(String inputString) {
		
		List<BasketItem> items = new ArrayList<BasketItem>();
		int maxrows = 10;
		
		
		try {

			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("BasketItemQueries.SuggestBasketItems"));
			ps.setString("desc", inputString + "%");
			ps.setInt("max_row", maxrows);
			ResultSet rs = ps.executeQuery();

			while (rs.next()){
				items.add(GetItemFromResultSet(rs));
			}			    
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
		
		return items;
	}

	public List<String> getUpcTypes() {
		
		List<String> upcTypes = new ArrayList<String>();
		
		try {

			PreparedStatement ps = connection.prepareStatement(DaoUtilities.getQueryFromXML("UIQueries.GetUpcTypes"));

			ResultSet rs = ps.executeQuery();

			while (rs.next()){
				upcTypes.add(rs.getString("upi_type_cd"));
			}			    
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
		
		return upcTypes;
	}
	
	private BasketItem GetItemFromResultSet(ResultSet rs) throws SQLException{
		
		BasketItem b = new BasketItem();
		
		for(int i = 1; i <= rs.getMetaData().getColumnCount(); i++){
			if(rs.getMetaData().getColumnName(i).equals("basket_item_id")){
				b.basketItemID = rs.getInt("basket_item_id");
			}else if(rs.getMetaData().getColumnName(i).equals("desc_txt")){
				b.description = rs.getString("desc_txt");
			}else if(rs.getMetaData().getColumnName(i).equals("upi_type_cd")){
				b.upc = new UPI();
				b.upc.upiCode = rs.getString("upi_type_cd");
				b.upc.upiType = rs.getString("upi_type_cd");
				b.upc.upiCode = rs.getString("upi_code_txt");
			}else if(rs.getMetaData().getColumnName(i).equals("")){
				
			}else if(rs.getMetaData().getColumnName(i).equals("")){
				
			}

		}
		
		return b;
	}
	
	@SuppressWarnings("unused")
	private Manufacturer GetManufacturerFromResultSet(ResultSet rs) throws SQLException{
		Manufacturer m = new Manufacturer();
		
		m.manufacturerID = rs.getInt("basket_item_id");
		return m;
	}


}
