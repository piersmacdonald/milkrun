package com.milkrun.server.dal.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Logger;

import com.milkrun.server.dal.utilities.NamedParameterStatement;
import com.milkrun.server.utils.DaoUtilities;
import com.milkrun.server.utils.NoSuchQueryException;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.RetailStore;
import com.milkrun.shared.dto.geo.Location;


public enum StoreDAO{
	INSTANCE;

	private static final Logger log = Logger.getLogger(StoreDAO.class.getName());
	private Connection connection = null;
	
	public void setConnection(Connection c){ connection = c; }
	public void closeConnection(){
		if (connection != null){ 
			try {
				connection.close();
	        }catch (SQLException ignore) {
	        }
		}
	}
	
	public List<RetailStore> listRetailStores() 
	{
		List<RetailStore> stores = new ArrayList<RetailStore>();

		try {

			PreparedStatement ps = connection.prepareStatement(DaoUtilities.getQueryFromXML("StoreQueries.listStores"));
	
			ResultSet rs = ps.executeQuery();

			while (rs.next()){
				stores.add(GetStoreFromResultSet(rs));
			}
					
		} catch (SQLException e) {
			e.printStackTrace();
			log.severe("Error listing items: " + e.toString());
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return stores;
	}

	public int addRetailStore(RetailStore store) 
	{
		int newID = 0;
		Random generator = new Random(); //todo: erase
		
		try {
			  String s = DaoUtilities.getQueryFromXML("StoreQueries.InsertRetailStore");
		      PreparedStatement ps = connection.prepareStatement(s, Statement.RETURN_GENERATED_KEYS);
		      ps.setString(1, store.storeName);
		      ps.setString(2, store.storeUniqueID);	
		      ps.setInt(3, 0);	
		      ps.setDouble(4, generator.nextDouble());
		      ps.setDouble(5, generator.nextDouble());	
		      ps.setString(6, store.location.fullAddress);	
		      ps.executeUpdate();
		      			
		      ResultSet rs = ps.getGeneratedKeys();
		      if (rs != null && rs.next()) {
		    	  newID = rs.getInt(1);
		      } 

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return newID;
	}
	
	public boolean addPricedItemToStore(RetailStore store, BasketItem item) throws SQLException, NoSuchQueryException 
	{		
		try {
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("StoreQueries.InsertItemIntoStoreMapping"));
	
			ps.setString("price_txt", item.price.toString());	
			ps.setInt("retail_store_id", store.retailStoreID);	
			ps.setInt("basket_item_id", item.basketItemID);
		    ps.executeUpdate();
	   			
		} catch (SQLException e) {
			e.printStackTrace();
			throw e;
			
		} catch (NoSuchQueryException e) {
			e.printStackTrace();
			throw e;
			
		} 
		
		return true;
	}
	
	public boolean updateRetailStore(RetailStore store) 
	{	
		try {
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("StoreQueries.UpdateRetailStore"));
			ps.setString("store_name_txt", store.storeName);
			ps.setString("store_num", store.storeUniqueID);	
			ps.setInt("address_id", 0);	
			ps.setString("address_txt", store.location.fullAddress);	
			ps.setInt("retail_store_id", store.retailStoreID);
			ps.executeUpdate();
		      			
		} catch (SQLException e) {
			e.printStackTrace();
			log.severe("updateRetailStore.StoreDAO - " + e.getLocalizedMessage());
			return false;
		} catch (NoSuchQueryException e) {
			e.printStackTrace();
			return false;
		} 
		return true;
	}
	
	public RetailStore getRetailStore(int retailStoreID) {

		try {
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("StoreQueries.GetStore"));
			ps.setInt("retail_store_id", retailStoreID);
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()){
				return GetStoreFromResultSet(rs);
			}
			
		} catch (SQLException e) {
			log.severe("Error getting basket: " + e.toString());
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return null;				
	}


	public List<RetailStore> suggestStore(String inputString) {
		
		List<RetailStore> stores = new ArrayList<RetailStore>();
		
		try {

			PreparedStatement ps = connection.prepareStatement(DaoUtilities.getQueryFromXML("StoreQueries.SuggestStore"));
			ps.setString(1, inputString + "%");

			ResultSet rs = ps.executeQuery();

			while (rs.next()){
				stores.add(GetStoreFromResultSet(rs));
			}			    
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return stores;
	}
	
	public List<BasketItem> getStoreInventory(RetailStore store) throws SQLException, NoSuchQueryException {
		
		List<BasketItem> items = null;
		List<Integer> itemIDs = new ArrayList<Integer>();
		Map<Integer, BigDecimal> itemPrice = new HashMap<Integer, BigDecimal>();
		
		try {
			NamedParameterStatement ps = new NamedParameterStatement(connection, DaoUtilities.getQueryFromXML("StoreQueries.GetItemInventoryByStoreID"));
			ps.setInt("retail_store_id", store.retailStoreID);
			ResultSet rs = ps.executeQuery();

			while (rs.next()){
				itemIDs.add(rs.getInt("basket_item_id"));	
				itemPrice.put(rs.getInt("basket_item_id"), new BigDecimal(rs.getString("price_txt")));
			}	
			
			//Don't bother doing this if there's no items
			if(itemIDs.size() > 0){
				ItemDAO.INSTANCE.setConnection(connection);
				items = ItemDAO.INSTANCE.getBasketItems(itemIDs);
				
				for(BasketItem b: items){
					b.price = itemPrice.get(b.basketItemID);
				}
			}

			
		} catch (SQLException e) {
			throw e;
		} catch (NoSuchQueryException e) {
			throw e;
		} 
		
		return items;
	}

	private RetailStore GetStoreFromResultSet(ResultSet rs) throws SQLException{
			RetailStore s = new RetailStore();
			s.retailStoreID = rs.getInt("retail_store_id");
			s.storeName = rs.getString("store_name_txt");
			s.storeUniqueID = rs.getString("store_num");
			s.retailStoreID = rs.getInt("retail_store_id");
			s.location = new Location();
			s.location.fullAddress = rs.getString("address_txt");
			return s;
	}
}
