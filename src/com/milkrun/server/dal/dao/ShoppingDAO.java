package com.milkrun.server.dal.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.milkrun.server.utils.DaoUtilities;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.RetailStore;
import com.milkrun.shared.dto.ShoppedBasket;
import com.milkrun.shared.dto.UserBasket;


public enum ShoppingDAO{
	INSTANCE;

	private static final Logger log = Logger.getLogger(ShoppingDAO.class.getName());
	private Connection connection = null;
	

	public void setConnection(Connection c){ connection = c; }
	

	public List<ShoppedBasket> shopBasket(UserBasket basket) {
		
		Map<Integer, BasketItem> items = new HashMap<Integer, BasketItem>();
		for(BasketItem b: basket.items){
			items.put(b.basketItemID, b);
		}
		
		Map<Integer, ShoppedBasket> shoppedBasketHash = new HashMap<Integer, ShoppedBasket>();
		BasketItem basketItem;
		ShoppedBasket sb;
		try {
				
			PreparedStatement ps = connection.prepareStatement(DaoUtilities.getINQuery("ShopBasketQueries.ShopUserBasket", basket.items));	     
			ResultSet rs = ps.executeQuery();
			
			while (rs.next()){
				int storeID = rs.getInt("retail_store_id");

				/*
				 * TODO: MASSSIVE!!!!!!!!!! implement proper copying for basketitem to save getting copies from the DB
				 * I tried in the BasketItem class (still visible, commented out) but got NoSuchMethodError
				 */
				
				basketItem = new BasketItem();
				basketItem.basketItemID = items.get(rs.getInt("basket_item_id")).basketItemID;
				basketItem.description = items.get(rs.getInt("basket_item_id")).description;
				basketItem.upc = items.get(rs.getInt("basket_item_id")).upc;
				basketItem.price = new BigDecimal(rs.getString("price_txt"));
				
				if(shoppedBasketHash.containsKey(storeID)){
					sb = shoppedBasketHash.get(storeID);
					sb.items.add(basketItem);
					shoppedBasketHash.put(storeID, sb);
				}else{
					sb = new ShoppedBasket();
					sb.store = new RetailStore();
					sb.store.retailStoreID = rs.getInt("retail_store_id");
					sb.store.storeName = rs.getString("store_name_txt");
					
					sb.items = new ArrayList<BasketItem>();
					sb.items.add(basketItem);
					
					shoppedBasketHash.put(storeID, sb);
				}
			}

		} catch (SQLException e) {
			log.severe(e.getMessage());
		} finally {
			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
	
		return new ArrayList<ShoppedBasket>(shoppedBasketHash.values());
		
	}
}
