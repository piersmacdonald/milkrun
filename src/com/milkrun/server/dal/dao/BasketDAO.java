package com.milkrun.server.dal.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.milkrun.server.utils.DaoUtilities;
import com.milkrun.server.utils.NoSuchQueryException;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.UserBasket;

public enum BasketDAO{
	INSTANCE;

	private static final Logger log = Logger.getLogger(BasketDAO.class.getName());
	private Connection connection = null;	
	
	public void setConnection(Connection c){ connection = c; }
	
	public List<UserBasket> listUserBaskets(String userID) 
	{
/*		List<BasketItem> items = new ArrayList<BasketItem>();
		Connection c = null;
		try {
			DriverManager.registerDriver(new AppEngineDriver());
			c = DriverManager.getConnection(DaoUtilities.gaeSQLConnectionString);
			PreparedStatement ps = c.prepareStatement("SELECT * FROM BASKET_ITEM");
	
			ResultSet rs = ps.executeQuery();

			while (rs.next()){
				BasketItem b = new BasketItem();
				b.description = rs.getString("SHORT_DESC_TXT");
				b.upc = rs.getString("UPC_TXT");
				items.add(b);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			log.severe("Error listing items: " + e.toString());
		} finally {
			if (c != null){ 
				try {
					c.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
		
		return items;
		 /**
   * The singleton instance of the database.
   *
  private static ContactDatabase instance;

  /**
   * Get the singleton instance of the contact database.
   *
   * @return the singleton instance
   *
  public static ContactDatabase get() {
    if (instance == null) {
      instance = new ContactDatabase();
    }
    return instance;
  }
		
		*
		*
		*/
		return null;
	}
		
	public int createUserBasket(UserBasket basket) {
		
		int newUserBasketID = 0;
		
		try {
			
		      PreparedStatement ps = connection.prepareStatement(DaoUtilities.getQueryFromXML("UserBasketQueries.InsertUserBasket"), Statement.RETURN_GENERATED_KEYS);
		      ps.setString(1, basket.userID);
		      ps.setString(2, basket.description);
		      ps.setString(3, basket.createUserId);
		      ps.setString(4, basket.lastUpdateUserId);
		      ps.executeUpdate();
		      
		      ResultSet rs = ps.getGeneratedKeys();
		      if (rs != null && rs.next()) {
		    	  newUserBasketID = rs.getInt(1);
		      } 

		      for(BasketItem b: basket.items){
		    	  ps.clearParameters();
		    	  ps = connection.prepareStatement(DaoUtilities.getQueryFromXML("UserBasketQueries.AddItemToBasket"));
		    	  
			      ps.setInt(1, newUserBasketID);
			      ps.setInt(2, b.basketItemID);
			      ps.setString(3, basket.createUserId);
			      ps.setString(4, basket.lastUpdateUserId);
			      ps.executeUpdate();
		      }
		      
		      
		      
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
		
		return newUserBasketID;
		
	}



	public void updateUserBasket(UserBasket basket) {
		
		try {
			
			PreparedStatement ps = connection.prepareStatement(DaoUtilities.getQueryFromXML(""));
			ps.setString(1, basket.createUserId);
			ps.setString(2, basket.description);	
			//ps.setString(2, basket.basketID);	
			ps.executeUpdate();
			
			for(BasketItem b : basket.items){
				//PreparedStatement ps = c.prepareStatement(DaoUtilities.getQuery("addItemToBasket"));
				ps.setInt(1, basket.userBasketID);
				ps.setInt(1, b.basketItemID);
				ps.executeUpdate();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
	}
	
	public UserBasket getUserBasket(String basketID) 
	{
		UserBasket b = new UserBasket();
		b.items = new ArrayList<BasketItem>();
		BasketItem tempItem;

		try {
			
			PreparedStatement ps = connection.prepareStatement(DaoUtilities.getQueryFromXML("UserBasketQueries.GetUserBasket"));
			ps.setString(1, basketID);
			ResultSet rs = ps.executeQuery();
			
			boolean isFirst = true;
			while (rs.next()){
				
				tempItem = new BasketItem();
				
				tempItem.basketItemID = rs.getInt("basket_item_id");
				tempItem.description = rs.getString("short_desc_txt");
				//tempItem.upc = rs.getString("upc_txt");
				b.items.add(tempItem);
				if(isFirst){
					b.userBasketID = rs.getInt("user_basket_id");
					b.description = rs.getString("basket_desc_txt");	
				}
				isFirst = false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
			log.severe("Error listing items: " + e.toString());
		} catch (NoSuchQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (connection != null){ 
				try {
					connection.close();
		        }catch (SQLException ignore) {
		        }
			}
		}
		
		return b;
	}
	
	public void deleteUserBasket(String basketID) 
	{
		
	}

}
