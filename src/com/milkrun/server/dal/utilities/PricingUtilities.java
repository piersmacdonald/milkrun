package com.milkrun.server.dal.utilities;

import java.math.BigDecimal;
import java.math.MathContext;

import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.ShoppedBasket;
import com.milkrun.shared.dto.ShoppingResults;

public class PricingUtilities {

	public static String gaeSQLConnectionString = "jdbc:google:rdbms://piersmacdonald.com:milkrun:mrdv01/milkrundb01";
	
	
	public static ShoppingResults calculateBasketPrices(ShoppingResults results){
		try{
		for(ShoppedBasket s: results.baskets){
			s.totalPrice = new BigDecimal(0);
			
			for(BasketItem b: s.items){
				s.totalPrice = s.totalPrice.add(b.price, MathContext.DECIMAL32);
			}
		}
		}catch(Exception e){System.out.println(e.getLocalizedMessage());}
		return results;
	}
}
