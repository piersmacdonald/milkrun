package com.milkrun.client;

import com.google.gwt.activity.shared.ActivityManager;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.place.shared.PlaceHistoryHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HeaderPanel;
import com.google.gwt.user.client.ui.RootLayoutPanel;
import com.milkrun.client.mvp.AppPlaceHistoryMapper;
import com.milkrun.client.mvp.MainActivityMapper;
import com.milkrun.client.place.AppNavPlace;
import com.milkrun.client.ui.AppHeaderView;
import com.milkrun.client.ui.OneWidgetLayoutPanel;
import com.milkrun.client.utilities.ClientConstants;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */

public class MilkRunWebApp implements EntryPoint {
	//interface Binder extends UiBinder<DockLayoutPanel, MilkRunWebApp> { }
	interface Binder extends UiBinder<HeaderPanel, MilkRunWebApp> { }
	
	private static final Binder binder = GWT.create(Binder.class);
	
	@UiField AppHeaderView headerPanel;
	@UiField OneWidgetLayoutPanel mainPanel;
	
	private Place defaultPlace = new AppNavPlace(ClientConstants.ACTIVITY_HOME);

	
	/**
	 * This is the entry point method.
	 */
	@Override
	public void onModuleLoad()
	{
		// Create app layout
	    //DockLayoutPanel outer = binder.createAndBindUi(this);
	    HeaderPanel outer = binder.createAndBindUi(this);
	    
	    // Create ClientFactory using deferred binding so we can replace with different
		// impls in gwt.xml
		ClientFactory clientFactory = GWT.create(ClientFactory.class);
		EventBus eventBus = clientFactory.getEventBus();
		PlaceController placeController = clientFactory.getPlaceController();
				
		// Start ActivityManager for each area with its ActivityMapper
		ActivityMapper mainPanelActivityMapper = new MainActivityMapper(clientFactory);
		ActivityManager mainPanelActivityManager = new ActivityManager(mainPanelActivityMapper, eventBus);
		mainPanelActivityManager.setDisplay(mainPanel);
		
		// Start PlaceHistoryHandler with our PlaceHistoryMapper
		AppPlaceHistoryMapper historyMapper= GWT.create(AppPlaceHistoryMapper.class);
		PlaceHistoryHandler historyHandler = new PlaceHistoryHandler(historyMapper);
		historyHandler.register(placeController, eventBus, defaultPlace);
		
		// Set ClientFactory to main toolbar
		headerPanel.setClientFactory(clientFactory);
	    
	    // Add app layout to root layout
	    RootLayoutPanel.get().add(outer);
	    
	    // Goes to place represented on URL or default place
		historyHandler.handleCurrentHistory();
		
		// Goes to place represented on URL or default place
		
	}
}
