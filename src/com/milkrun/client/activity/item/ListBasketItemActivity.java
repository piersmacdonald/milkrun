package com.milkrun.client.activity.item;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.ui.items.ListBasketItemView;


public class ListBasketItemActivity extends AbstractActivity {
	private ClientFactory clientFactory;


	public ListBasketItemActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		ListBasketItemView listBasketItemView = clientFactory.getListBasketItemView();
		listBasketItemView.fillTable();
		containerWidget.setWidget(listBasketItemView.asWidget());		
	}
}