package com.milkrun.client.activity.item;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.item.EditBasketItemPlace;
import com.milkrun.client.ui.items.EditBasketItemView;

public class EditBasketItemActivity extends AbstractActivity {
	// Used to obtain views, eventBus, placeController
	// Alternatively, could be injected via GIN
	private ClientFactory clientFactory;
	// Name that will be appended to "Hello,"
	private String id;

	public EditBasketItemActivity(EditBasketItemPlace place, ClientFactory clientFactory) {
		this.id = place.getBasketItemId();
		this.clientFactory = clientFactory;
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		EditBasketItemView editItemView = clientFactory.getEditBasketItemView();
		editItemView.fillTable();
		containerWidget.setWidget(editItemView.asWidget());
	}

	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}

}
