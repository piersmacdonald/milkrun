package com.milkrun.client.activity.item;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.item.BasketItemPlace;
import com.milkrun.client.ui.items.AddBasketItemView;
import com.milkrun.client.ui.items.EditBasketItemView;


public class BasketItemActivity extends AbstractActivity {
	
	private ClientFactory clientFactory;
	private String mode;

	public BasketItemActivity(BasketItemPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.mode = place.getMode();
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {

		if(mode.equalsIgnoreCase("add")){
			AddBasketItemView addItemView = clientFactory.getAddBasketItemView();
			addItemView.setClientFactory(clientFactory);
			containerWidget.setWidget(addItemView.asWidget());
		}
		else if(mode.equalsIgnoreCase("add")){
			EditBasketItemView editItemView = clientFactory.getEditBasketItemView();
			containerWidget.setWidget(editItemView.asWidget());
		}
			
	}


	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}