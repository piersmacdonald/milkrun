package com.milkrun.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.AppNavPlace;
import com.milkrun.client.ui.AppAdminView;
import com.milkrun.client.ui.AppHomeView;
import com.milkrun.client.ui.CommunityMainView;
import com.milkrun.client.utilities.ClientConstants;


public class AppNavActivity extends AbstractActivity {
	private ClientFactory clientFactory;
	
	private String mode;

	public AppNavActivity(AppNavPlace place, ClientFactory clientFactory) {
		
		this.mode = place.getMode();
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		if(mode.equals(ClientConstants.ACTIVITY_COMMUNITY))
		{
			CommunityMainView view = clientFactory.getCommunityMainView();
			view.setClientFactory(clientFactory);
			containerWidget.setWidget(view.asWidget());
		}
		else if (mode.equals(ClientConstants.ACTIVITY_ADMIN))
		{
			AppAdminView view = clientFactory.getAppAdminView();
			containerWidget.setWidget(view.asWidget());
		}
		else if (mode.equals(ClientConstants.ACTIVITY_HOME))
		{
			AppHomeView view = clientFactory.getAppHomeView();
			view.setWelcomeMessage();
			containerWidget.setWidget(view.asWidget());
		}

	}


	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}