package com.milkrun.client.activity.store;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.ui.store.StoreItemMapView;


public class StoreItemMapActivity extends AbstractActivity {
	private ClientFactory clientFactory;


	public StoreItemMapActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		StoreItemMapView storeItemMapView = clientFactory.getStoreItemMapView();
		storeItemMapView.setClientFactory(clientFactory);
		containerWidget.setWidget(storeItemMapView.asWidget());		
	}
}