package com.milkrun.client.activity.store;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.store.StorePlace;
import com.milkrun.client.ui.store.AddStoreView;
import com.milkrun.client.ui.store.ListStoreView;
import com.milkrun.client.utilities.ClientConstants;


public class StoreActivity extends AbstractActivity {
	private ClientFactory clientFactory;

	private String id;

	public StoreActivity(StorePlace place, ClientFactory clientFactory) {
		this.id = place.getAction();
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		if(id.equals(ClientConstants.ACTIVITY_ADD)){
			AddStoreView addStoreView = clientFactory.getAddStoreView();
			containerWidget.setWidget(addStoreView.asWidget());
		}else if(id.equals(ClientConstants.ACTIVITY_LIST)){
			ListStoreView listStoreView = clientFactory.getListStoreView();
			listStoreView.setClientFactory(clientFactory);
			listStoreView.fillTable();
			containerWidget.setWidget(listStoreView.asWidget());			
		}

	}
	


	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}