package com.milkrun.client.activity.store;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.store.EditStorePlace;
import com.milkrun.client.ui.store.EditStoreView;

public class EditStoreActivity extends AbstractActivity {

	private ClientFactory clientFactory;

	private String id;

	public EditStoreActivity(EditStorePlace place, ClientFactory clientFactory) {
		this.id = place.getRetailStoreId();
		this.clientFactory = clientFactory;
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		EditStoreView editStoreView = clientFactory.getEditStoreView();
		editStoreView.setClientFactory(clientFactory);
		editStoreView.refresh(id);
		containerWidget.setWidget(editStoreView.asWidget());
	}

	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}


	
}
