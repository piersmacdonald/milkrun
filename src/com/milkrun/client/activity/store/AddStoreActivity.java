package com.milkrun.client.activity.store;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.store.AddStorePlace;
import com.milkrun.client.ui.store.AddStoreView;


public class AddStoreActivity extends AbstractActivity {
	private ClientFactory clientFactory;


	public AddStoreActivity(AddStorePlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		AddStoreView addStoreView = clientFactory.getAddStoreView();
		containerWidget.setWidget(addStoreView.asWidget());
	}
	


	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}