package com.milkrun.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.item.AddBasketItemPlace;
import com.milkrun.client.ui.items.AddBasketItemView;


public class UserAdminActivity extends AbstractActivity {
	private ClientFactory clientFactory;


	public UserAdminActivity(AddBasketItemPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		AddBasketItemView addItemView = clientFactory.getAddBasketItemView();
		addItemView.setClientFactory(clientFactory);
		containerWidget.setWidget(addItemView.asWidget());
	}


	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}