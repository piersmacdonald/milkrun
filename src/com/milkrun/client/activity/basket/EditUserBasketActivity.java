package com.milkrun.client.activity.basket;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.basket.EditUserBasketPlace;
import com.milkrun.client.ui.basket.EditUserBasketView;

public class EditUserBasketActivity extends AbstractActivity {
	// Used to obtain views, eventBus, placeController
	// Alternatively, could be injected via GIN
	private ClientFactory clientFactory;
	// Name that will be appended to "Hello,"
	private String id;

	public EditUserBasketActivity(EditUserBasketPlace place, ClientFactory clientFactory) {
		this.id = place.getUserBasketId();
		this.clientFactory = clientFactory;
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		EditUserBasketView editBasketView = clientFactory.getEditUserBasketView();
		editBasketView.setClientFactory(clientFactory);
		containerWidget.setWidget(editBasketView.asWidget());
	}

	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}


	
}
