package com.milkrun.client.activity.basket;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.basket.ShopSelectedBasketPlace;
import com.milkrun.client.ui.basket.ShopSelectedBasketView;


public class ShopSelectedBasketActivity extends AbstractActivity {
	private ClientFactory clientFactory;
	private String basketID;

	public ShopSelectedBasketActivity(ShopSelectedBasketPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		this.basketID = place.getBasketID();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		ShopSelectedBasketView mapBasketView = clientFactory.getMapSelectedBasketView();
		mapBasketView.setClientFactory(clientFactory);
		mapBasketView.getShoppedBasketResults(basketID);
		panel.setWidget(mapBasketView.asWidget());
	}


	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}