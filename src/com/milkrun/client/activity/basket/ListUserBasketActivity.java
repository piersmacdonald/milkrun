package com.milkrun.client.activity.basket;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.basket.ListUserBasketPlace;
import com.milkrun.client.ui.basket.ListUserBasketView;


public class ListUserBasketActivity extends AbstractActivity {
	private ClientFactory clientFactory;


	public ListUserBasketActivity(ListUserBasketPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		ListUserBasketView listUserBasketView = clientFactory.getListUserBasketView();
		listUserBasketView.setClientFactory(clientFactory);
		listUserBasketView.fillTable();
		containerWidget.setWidget(listUserBasketView.asWidget());		
	}
	


	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}