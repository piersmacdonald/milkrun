package com.milkrun.client.activity.basket;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.basket.AddUserBasketPlace;
import com.milkrun.client.ui.basket.CreateUserBasketView;


public class AddUserBasketActivity extends AbstractActivity {
	private ClientFactory clientFactory;


	public AddUserBasketActivity(AddUserBasketPlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		CreateUserBasketView addBasketView = clientFactory.getAddUserBasketView();
		addBasketView.setClientFactory(clientFactory);
		containerWidget.setWidget(addBasketView.asWidget());
	}
	


	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}
}