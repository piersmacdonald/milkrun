package com.milkrun.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.ui.AppHeaderView;



public class HeaderActivity extends AbstractActivity {
	private ClientFactory clientFactory;


	public HeaderActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		AppHeaderView headerView = clientFactory.getAppHeaderView();
		headerView.setClientFactory(clientFactory);
		containerWidget.setWidget(headerView.asWidget());
	}
	
	public void addItem(String desc, String brandName) {

	}
	
	/**
	 * Ask user before stopping this activity
	 */
	@Override
	public String mayStop() {
		return "Please hold on. This activity is stopping.";
	}

	/**
	 * Navigate to a new Place in the browser
	 */
	public void goTo(Place place) {
		clientFactory.getPlaceController().goTo(place);
	}

}