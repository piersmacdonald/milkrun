package com.milkrun.client.utilities;

public class ClientConstants {

    public static final int ITEMS_TABLE_MAX_ROWS = 15;

    public static final String ACTIVITY_ADD = "add";
    public static final String ACTIVITY_LIST = "list";
    public static final String ACTIVITY_COMMUNITY = "community";
    public static final String ACTIVITY_ADMIN = "admin";
    public static final String ACTIVITY_HOME = "home";
    
    public static final int SHOPBASKET_STACKPANEL_WIDTH = 200;
    public static final int SHOPBASKET_STACKPANEL_HEIGHT = 500;
    public static final int SHOPBASKET_STACKPANEL_HEADERHEIGHT = 50;
    
}