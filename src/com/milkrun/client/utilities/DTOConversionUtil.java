package com.milkrun.client.utilities;

import com.google.web.bindery.autobean.shared.AutoBean;
import com.google.web.bindery.autobean.shared.AutoBeanCodex;
import com.google.web.bindery.autobean.shared.AutoBeanFactory;
import com.google.web.bindery.autobean.shared.AutoBeanUtils;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.ItemSuggestion;
import com.milkrun.shared.dto.UserBasket;

public class DTOConversionUtil {

	String serializeBasketItemToJson(BasketItem item) 
	{
	    // Retrieve the AutoBean controller
	    AutoBean<BasketItem> bean = AutoBeanUtils.getAutoBean(item);
	    return AutoBeanCodex.encode(bean).getPayload();
	}
	
	String serializeUserBasketToJson(UserBasket basket) 
	{
	    // Retrieve the AutoBean controller
	    AutoBean<UserBasket> bean = AutoBeanUtils.getAutoBean(basket);
	    return AutoBeanCodex.encode(bean).getPayload();
	}

	BasketItem deserializeBasketItemFromJson(String json, AutoBeanFactory factory) 
	{     
	    AutoBean<BasketItem> bean = AutoBeanCodex.decode(factory, BasketItem.class, json);     
	    return bean.as();   
	} 
	
	public static ItemSuggestion ItemSuggestionFromJson(String json, AutoBeanFactory factory) 
	{     
	    AutoBean<ItemSuggestion> bean = AutoBeanCodex.decode(factory, ItemSuggestion.class, json);     
	    return bean.as();   
	} 
}
