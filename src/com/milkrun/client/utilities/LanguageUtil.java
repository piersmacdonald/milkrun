package com.milkrun.client.utilities;

import java.util.HashMap;
import java.util.Map;

public class LanguageUtil {

    private static final Map<String, String> dictionary = new HashMap<String, String>();
    static {
    	dictionary.put("createBasketViewLinkEN", "Create User Basket");
    	dictionary.put("adminViewLinkEN", "Admin");
    	dictionary.put("addItemViewLinkEN", "Add Basket Item");
    }

	public static String getPhrase(String phrase, String langCode){
		try{
			return dictionary.get(phrase+ langCode);
		}
		catch(Exception e){
			return "n/a";
		}
	}
}