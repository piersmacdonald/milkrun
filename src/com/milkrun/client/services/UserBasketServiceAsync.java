package com.milkrun.client.services;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.milkrun.shared.dto.ShoppingResults;
import com.milkrun.shared.dto.UserBasket;

/**
 * The async counterpart of <code>UserBasketService</code>.
 */
public interface UserBasketServiceAsync {
	
	void createUserBasket(UserBasket input, AsyncCallback<String> callback);
	
	void removeUserBasket(String input, AsyncCallback<String> callback);
	
	void getUserBasket(String input, AsyncCallback<UserBasket> callback);
	
	void updateUserBasket(UserBasket input, AsyncCallback<String> callback);
	
	void listUserBaskets(String userID, AsyncCallback<List<UserBasket>> callback);
	
	void shopUserBasket(String basketID, AsyncCallback<ShoppingResults> callback);
	
}
