package com.milkrun.client.services;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.milkrun.shared.dto.ShoppingResults;
import com.milkrun.shared.dto.UserBasket;

/*
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("basket")
public interface UserBasketService extends RemoteService {
	
	String removeUserBasket(String ID);
    
    UserBasket getUserBasket(String ID);

    String updateUserBasket(UserBasket basket);
    
    List<UserBasket> listUserBaskets(String user);

    String createUserBasket(UserBasket basket);
    
    ShoppingResults shopUserBasket(String basketID);
}


