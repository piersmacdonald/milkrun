package com.milkrun.client.services;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.milkrun.shared.dto.BasketItem;

/*
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("item")
public interface BasketItemService extends RemoteService {
	
	List<BasketItem> listBasketItems();
	
	String createBasketItem(BasketItem item) throws IllegalArgumentException;

	String removeBasketItem(String basketItemID) throws IllegalArgumentException;
  
    BasketItem getBasketItem(String basketItemID) throws IllegalArgumentException;

    String updateBasketItem(BasketItem item) throws IllegalArgumentException;
    
    List<String> getUpcType();
}


