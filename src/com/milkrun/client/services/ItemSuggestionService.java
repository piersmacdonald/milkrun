package com.milkrun.client.services;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.milkrun.shared.dto.ItemSuggestion;

/*
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("itemsuggestion")
public interface ItemSuggestionService extends RemoteService {
	ItemSuggestion suggestByString(String name) throws IllegalArgumentException;
}


