package com.milkrun.client.services;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.milkrun.shared.dto.ItemSuggestion;

/**
 * The async counterpart of <code>ItemSuggestionService</code>.
 */
public interface ItemSuggestionServiceAsync {
	void suggestByString(String input, AsyncCallback<ItemSuggestion> callback)
			throws IllegalArgumentException;
}
