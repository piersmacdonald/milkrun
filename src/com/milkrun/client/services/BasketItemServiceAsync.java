package com.milkrun.client.services;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.milkrun.shared.dto.BasketItem;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface BasketItemServiceAsync {
	
	void listBasketItems(AsyncCallback<List<BasketItem>> callback);
	
	void createBasketItem(BasketItem item, AsyncCallback<String> callback)
	throws IllegalArgumentException;
	
	void removeBasketItem(String basketItemID, AsyncCallback<String> callback)
	throws IllegalArgumentException;
	
	void getBasketItem(String basketItemID, AsyncCallback<BasketItem> callback)
	throws IllegalArgumentException;
	
	void updateBasketItem(BasketItem item, AsyncCallback<String> callback)
	throws IllegalArgumentException;
	
	void getUpcType(AsyncCallback<List<String>> callback);
}
