package com.milkrun.client.services;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.RetailStore;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface StoreServiceAsync {
	
	void listRetailStores(AsyncCallback<List<RetailStore>> callback);
	
	void createRetailStore(RetailStore store, AsyncCallback<String> callback)
	throws IllegalArgumentException;
	
	void removeRetailStore(String basketItemID, AsyncCallback<String> callback)
	throws IllegalArgumentException;
	
	void getRetailStore(String storeID, AsyncCallback<RetailStore> callback)
	throws IllegalArgumentException;
	
	void suggestStoreByString(String searchString, AsyncCallback<List<RetailStore>> callback)
	throws IllegalArgumentException;
	
	void updateRetailStore(RetailStore store, AsyncCallback<String> callback)
	throws IllegalArgumentException;
	
	void mapItemToStore(RetailStore store, BasketItem item, AsyncCallback<String> callback)
	throws Exception;
	
	void getPricedItems(RetailStore store, AsyncCallback<List<BasketItem>> callback) 
	throws Exception;
}
