package com.milkrun.client.services;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.RetailStore;

/*
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("retailstore")
public interface StoreService extends RemoteService {
	
	List<RetailStore> listRetailStores();
	
	String createRetailStore(RetailStore store) throws IllegalArgumentException;

	String removeRetailStore(String storeID) throws IllegalArgumentException;
  
	RetailStore getRetailStore(String storeID) throws IllegalArgumentException;

    List<RetailStore> suggestStoreByString(String searchString) throws IllegalArgumentException;
    
    String updateRetailStore(RetailStore store) throws IllegalArgumentException;
    
    String mapItemToStore(RetailStore store, BasketItem item) throws Exception;
    
    List<BasketItem> getPricedItems(RetailStore store) throws Exception;
}


