package com.milkrun.client.place.basket;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class ShopSelectedBasketPlace extends Place
{
	private String basketID;
	
    public ShopSelectedBasketPlace(String token)
    {
            this.basketID = token;
    }

    public String getBasketID()
    {
            return basketID;
    }

	public static class Tokenizer implements PlaceTokenizer<ShopSelectedBasketPlace>
	{
		@Override
		public String getToken(ShopSelectedBasketPlace place)
		{
			return place.getBasketID();
		}

		@Override
		public ShopSelectedBasketPlace getPlace(String token)
		{
			return new ShopSelectedBasketPlace(token);
		}

	}
	
}
