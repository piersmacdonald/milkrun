package com.milkrun.client.place.basket;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class AddUserBasketPlace extends Place
{


	public static class Tokenizer implements PlaceTokenizer<AddUserBasketPlace>
	{
		@Override
		public String getToken(AddUserBasketPlace place)
		{
			return "";
		}

		@Override
		public AddUserBasketPlace getPlace(String token)
		{
			return new AddUserBasketPlace();
		}
	}
	
}
