package com.milkrun.client.place.basket;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class ViewShoppedUserBasketPlace extends Place
{
	private String id;
	
	public ViewShoppedUserBasketPlace(String token)
	{
		this.id = token;
	}

	public String getBasketItemID()
	{
		return id;
	}

	public static class Tokenizer implements PlaceTokenizer<ViewShoppedUserBasketPlace>
	{

		@Override
		public String getToken(ViewShoppedUserBasketPlace place)
		{
			return place.getBasketItemID();
		}

		@Override
		public ViewShoppedUserBasketPlace getPlace(String token)
		{
			return new ViewShoppedUserBasketPlace(token);
		}

	}
	
//	@Override
//	protected Place getPlace(String token)
//	{
//		return new HelloPlace(token);
//	}
//
//	@Override
//	protected Activity getActivity()
//	{
//		return new HelloActivity("David");
//	}
}
