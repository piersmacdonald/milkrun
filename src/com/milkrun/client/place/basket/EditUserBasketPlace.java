package com.milkrun.client.place.basket;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class EditUserBasketPlace extends Place
{	
	private String id;
	
	public EditUserBasketPlace(String token)
	{
		this.id = token;
	}

	public String getUserBasketId()
	{
		return id;
	}

	public static class Tokenizer implements PlaceTokenizer<EditUserBasketPlace>
	{

		@Override
		public String getToken(EditUserBasketPlace place)
		{
			return place.getUserBasketId();
		}

		@Override
		public EditUserBasketPlace getPlace(String token)
		{
			return new EditUserBasketPlace(token);
		}

	}
}
