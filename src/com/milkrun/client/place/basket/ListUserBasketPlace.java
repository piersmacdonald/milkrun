package com.milkrun.client.place.basket;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class ListUserBasketPlace extends Place
{	

	public static class Tokenizer implements PlaceTokenizer<ListUserBasketPlace>
	{

		@Override
		public String getToken(ListUserBasketPlace place)
		{
			return "";
		}

		@Override
		public ListUserBasketPlace getPlace(String token)
		{
			return new ListUserBasketPlace();
		}

	}
}
