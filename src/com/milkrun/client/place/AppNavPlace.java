package com.milkrun.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class AppNavPlace extends Place
{
	private String mode;
	
	public AppNavPlace(String token)
	{
		this.mode = token;
	}

	public String getMode()
	{
		return mode;
	}
	

	public static class Tokenizer implements PlaceTokenizer<AppNavPlace>
	{
		@Override
		public String getToken(AppNavPlace place)
		{
			return place.getMode();
		}

		@Override
		public AppNavPlace getPlace(String token)
		{
			return new AppNavPlace(token);
		}
	}
	
}
