package com.milkrun.client.place.store;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class BulkStoreItemMapPlace extends Place
{	
	public static class Tokenizer implements PlaceTokenizer<BulkStoreItemMapPlace>
	{

		@Override
		public String getToken(BulkStoreItemMapPlace place)
		{
			return "";
		}

		@Override
		public BulkStoreItemMapPlace getPlace(String token)
		{
			return new BulkStoreItemMapPlace();
		}
	}
}
