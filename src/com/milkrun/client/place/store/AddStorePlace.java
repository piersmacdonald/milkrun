package com.milkrun.client.place.store;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class AddStorePlace extends Place
{
	public static class Tokenizer implements PlaceTokenizer<AddStorePlace>
	{
		@Override
		public String getToken(AddStorePlace place)
		{
			return "";
		}

		@Override
		public AddStorePlace getPlace(String token)
		{
			return new AddStorePlace();
		}
	}
}
