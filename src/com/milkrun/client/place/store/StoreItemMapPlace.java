package com.milkrun.client.place.store;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class StoreItemMapPlace extends Place
{	
	public static class Tokenizer implements PlaceTokenizer<StoreItemMapPlace>
	{

		@Override
		public String getToken(StoreItemMapPlace place)
		{
			return "";
		}

		@Override
		public StoreItemMapPlace getPlace(String token)
		{
			return new StoreItemMapPlace();
		}
	}
}
