package com.milkrun.client.place.store;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class EditStorePlace extends Place
{
	private String id;
	
	public EditStorePlace(String token)
	{
		this.id = token;
	}

	public String getRetailStoreId()
	{
		return id;
	}

	public static class Tokenizer implements PlaceTokenizer<EditStorePlace>
	{

		@Override
		public String getToken(EditStorePlace place)
		{
			return place.getRetailStoreId();
		}

		@Override
		public EditStorePlace getPlace(String token)
		{
			return new EditStorePlace(token);
		}

	}
}
