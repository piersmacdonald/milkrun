package com.milkrun.client.place.store;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class StorePlace extends Place
{
	private String id;
	
	public StorePlace(String token)
	{
		this.id = token;
	}

	public String getAction()
	{
		return id;
	}

	public static class Tokenizer implements PlaceTokenizer<StorePlace>
	{

		@Override
		public String getToken(StorePlace place)
		{
			return place.getAction();
		}

		@Override
		public StorePlace getPlace(String token)
		{
			return new StorePlace(token);
		}

	}
}
