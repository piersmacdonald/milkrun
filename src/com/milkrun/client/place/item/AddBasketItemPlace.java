package com.milkrun.client.place.item;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class AddBasketItemPlace extends Place
{
	public static class Tokenizer implements PlaceTokenizer<AddBasketItemPlace>
	{
		@Override
		public String getToken(AddBasketItemPlace place)
		{
			return "";
		}

		@Override
		public AddBasketItemPlace getPlace(String token)
		{
			return new AddBasketItemPlace();
		}
	}
}
