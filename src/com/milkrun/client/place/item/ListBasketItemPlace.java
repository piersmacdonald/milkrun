package com.milkrun.client.place.item;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class ListBasketItemPlace extends Place
{	
	public static class Tokenizer implements PlaceTokenizer<ListBasketItemPlace>
	{

		@Override
		public String getToken(ListBasketItemPlace place)
		{
			return "";
		}

		@Override
		public ListBasketItemPlace getPlace(String token)
		{
			return new ListBasketItemPlace();
		}
	}
}
