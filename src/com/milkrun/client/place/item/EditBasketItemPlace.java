package com.milkrun.client.place.item;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

//public class HelloPlace extends ActivityPlace<HelloActivity>
public class EditBasketItemPlace extends Place
{
	private String id;
	
	public EditBasketItemPlace(String token)
	{
		this.id = token;
	}

	public String getBasketItemId()
	{
		return id;
	}

	public static class Tokenizer implements PlaceTokenizer<EditBasketItemPlace>
	{

		@Override
		public String getToken(EditBasketItemPlace place)
		{
			return place.getBasketItemId();
		}

		@Override
		public EditBasketItemPlace getPlace(String token)
		{
			return new EditBasketItemPlace(token);
		}

	}
	
//	@Override
//	protected Place getPlace(String token)
//	{
//		return new HelloPlace(token);
//	}
//
//	@Override
//	protected Activity getActivity()
//	{
//		return new HelloActivity("David");
//	}
}
