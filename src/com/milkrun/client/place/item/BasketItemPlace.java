package com.milkrun.client.place.item;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class BasketItemPlace extends Place
{
	private String mode;
	
	public BasketItemPlace(String token)
	{
		this.mode = token;
	}

	public String getMode()
	{
		return mode;
	}
	

	public static class Tokenizer implements PlaceTokenizer<BasketItemPlace>
	{
		@Override
		public String getToken(BasketItemPlace place)
		{
			return place.getMode();
		}

		@Override
		public BasketItemPlace getPlace(String token)
		{
			return new BasketItemPlace(token);
		}
	}
	
}
