package com.milkrun.client.mvp;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.activity.AppNavActivity;
import com.milkrun.client.activity.basket.AddUserBasketActivity;
import com.milkrun.client.activity.basket.EditUserBasketActivity;
import com.milkrun.client.activity.basket.ListUserBasketActivity;
import com.milkrun.client.activity.basket.ShopSelectedBasketActivity;
import com.milkrun.client.activity.item.AddBasketItemActivity;
import com.milkrun.client.activity.item.EditBasketItemActivity;
import com.milkrun.client.activity.item.ListBasketItemActivity;
import com.milkrun.client.activity.store.AddStoreActivity;
import com.milkrun.client.activity.store.EditStoreActivity;
import com.milkrun.client.activity.store.StoreActivity;
import com.milkrun.client.activity.store.StoreItemMapActivity;
import com.milkrun.client.place.AppNavPlace;
import com.milkrun.client.place.basket.AddUserBasketPlace;
import com.milkrun.client.place.basket.EditUserBasketPlace;
import com.milkrun.client.place.basket.ListUserBasketPlace;
import com.milkrun.client.place.basket.ShopSelectedBasketPlace;
import com.milkrun.client.place.item.AddBasketItemPlace;
import com.milkrun.client.place.item.EditBasketItemPlace;
import com.milkrun.client.place.item.ListBasketItemPlace;
import com.milkrun.client.place.store.AddStorePlace;
import com.milkrun.client.place.store.EditStorePlace;
import com.milkrun.client.place.store.StoreItemMapPlace;
import com.milkrun.client.place.store.StorePlace;


public class MainActivityMapper implements ActivityMapper {

	private ClientFactory clientFactory;

	/**
	 * AppActivityMapper associates each Place with its corresponding
	 * {@link Activity}
	 * 
	 * @param clientFactory
	 *            Factory to be passed to activities
	 */
	public MainActivityMapper(ClientFactory clientFactory) {
		super();
		this.clientFactory = clientFactory;
	}

	/**
	 * Map each Place to its corresponding Activity. This would be a great use
	 * for GIN.
	 */
	@Override
	public Activity getActivity(Place place) {
		// This is begging for GIN
		if (place instanceof EditBasketItemPlace)
			return new EditBasketItemActivity((EditBasketItemPlace) place, clientFactory);
		else if (place instanceof AddBasketItemPlace)
			return new AddBasketItemActivity(clientFactory);
		else if (place instanceof ListBasketItemPlace)
			return new ListBasketItemActivity(clientFactory);
		
		else if (place instanceof AddUserBasketPlace)
			return new AddUserBasketActivity((AddUserBasketPlace) place, clientFactory);
		else if (place instanceof EditUserBasketPlace)
			return new EditUserBasketActivity((EditUserBasketPlace) place, clientFactory);
		else if (place instanceof ListUserBasketPlace)
			return new ListUserBasketActivity((ListUserBasketPlace) place, clientFactory);
		else if (place instanceof ShopSelectedBasketPlace)
			return new ShopSelectedBasketActivity((ShopSelectedBasketPlace) place, clientFactory);	
		
		else if (place instanceof AddStorePlace)
			return new AddStoreActivity((AddStorePlace) place, clientFactory);
		else if (place instanceof StorePlace)
			return new StoreActivity((StorePlace) place, clientFactory);
		else if (place instanceof EditStorePlace)
			return new EditStoreActivity((EditStorePlace) place, clientFactory);
		else if (place instanceof StoreItemMapPlace)
			return new StoreItemMapActivity(clientFactory);
		
		else if (place instanceof AppNavPlace)
			return new AppNavActivity((AppNavPlace) place, clientFactory);	
		else
			return null;
	}

}
