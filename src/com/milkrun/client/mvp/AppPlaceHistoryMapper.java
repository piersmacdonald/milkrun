package com.milkrun.client.mvp;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;
import com.milkrun.client.place.basket.AddUserBasketPlace;
import com.milkrun.client.place.item.AddBasketItemPlace;
import com.milkrun.client.place.item.EditBasketItemPlace;


/**
 * PlaceHistoryMapper interface is used to attach all places which the
 * PlaceHistoryHandler should be aware of. This is done via the @WithTokenizers
 * annotation or by extending PlaceHistoryMapperWithFactory and creating a
 * separate TokenizerFactory.
 */
@WithTokenizers( { 	EditBasketItemPlace.Tokenizer.class, 
					AddBasketItemPlace.Tokenizer.class,
					AddUserBasketPlace.Tokenizer.class})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
