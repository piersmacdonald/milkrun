package com.milkrun.client.ui.basket;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;

public class ListUserBasketView extends Composite 
{
	private static ListUserBasketViewUiBinder uiBinder = GWT.create(ListUserBasketViewUiBinder.class);	
	interface ListUserBasketViewUiBinder extends UiBinder<Widget, ListUserBasketView>	{}
    private ClientFactory clientFactory;

/*	@UiField(provided = true) CellTable<ContactProxy> table;
	@UiField Label counter;*/
	
	public ListUserBasketView()
	{
		createTable();
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void fillTable() {
/*		if(clientFactory != null) {
			clientFactory.getRequestFactory().contactRequest().generateContacts().fire(
					new Receiver<Void>() {
						@Override
						public void onSuccess(Void response) {
							updateTable();
						}
					}
			);
		}*/
	}
	
	private void updateTable() {
/*		if(clientFactory != null) {
			clientFactory.getRequestFactory().contactRequest().findAllContacts().fire(
					new Receiver<List<ContactProxy>>() {

						@Override
						public void onSuccess(List<ContactProxy> response) {
							//Set CONTACTS List
							CONTACTS = response;
							// Set the total row count
							table.setRowCount(CONTACTS.size(), true);
							// Push the data into the widget.
							table.setRowData(0, CONTACTS);
							// Set counter label
							counter.setText("Total of contacts: " + ((Integer)CONTACTS.size()).toString());
						}
						
					}
			);
		}	*/	
	}

	private void createTable() {
/*		table = new CellTable<ContactProxy>();
		table.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

		// Create name column
		TextColumn<ContactProxy> nameColumn = new TextColumn<ContactProxy>() {
			@Override
			public String getValue(ContactProxy object) {
				return object.getName();
			}
		};
		
		// Create address column
		TextColumn<ContactProxy> addressColumn = new TextColumn<ContactProxy>() {
			@Override
			public String getValue(ContactProxy object) {
				return object.getAddress();
			}
		};		
		
		table.addColumn(nameColumn, "Name");
		
		table.addColumn(addressColumn, "Address");

		// Add a selection model to handle user selection.
		final SingleSelectionModel<ContactProxy> selectionModel = new SingleSelectionModel<ContactProxy>();
		table.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					public void onSelectionChange(SelectionChangeEvent event) {
						ContactProxy selected = selectionModel.getSelectedObject();
						if (selected != null) {
							clientFactory.setSelectedContactId(selected.getId().toString());
						}
					}
				});*/
	}      
	
	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}

}
