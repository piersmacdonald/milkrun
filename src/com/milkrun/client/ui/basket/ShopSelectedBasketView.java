package com.milkrun.client.ui.basket;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.maps.client.MapOptions;
import com.google.gwt.maps.client.MapTypeId;
import com.google.gwt.maps.client.MapWidget;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.StackLayoutPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.basket.AddUserBasketPlace;
import com.milkrun.client.services.UserBasketService;
import com.milkrun.client.services.UserBasketServiceAsync;
import com.milkrun.client.utilities.ClientConstants;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.ShoppedBasket;
import com.milkrun.shared.dto.ShoppingResults;
import com.milkrun.shared.dto.UserBasket;

public class ShopSelectedBasketView extends Composite
{

	private static MapSelectedBasketViewUiBinder uiBinder = GWT.create(MapSelectedBasketViewUiBinder.class);
	interface MapSelectedBasketViewUiBinder extends UiBinder<Widget, ShopSelectedBasketView>{}
	
	private final UserBasketServiceAsync basketService = GWT.create(UserBasketService.class);
	private MapWidget map;
	
	//@UiField SimplePanel mapContainerPanel;
	@UiField SimplePanel basketStackPanelContainer;
	//@UiField StackLayoutPanel basketStackPanel;
	@UiField Button backButton;
	
	private List<ShoppedBasket> BASKETS;
	private ClientFactory clientFactory;
	private UserBasket basket = new UserBasket();
	private StackLayoutPanel basketStackPanel;
	
	public ShopSelectedBasketView()
	{
		initWidget(uiBinder.createAndBindUi(this));	
		createStackPanel();
		
		//initMap();
/*		Maps.loadMapsApi("ABQIAAAAu-msj8nCdUsUfFWPPfV9sxQISW_qaAbyDpwNQltog7FoBSFGjBRV35JJ5STIqWcju7LKjpwyBMeqNQ", "2", false, new Runnable(){
			public void run() {
				initMap();
			}
		});*/
	}
	
	private void createStackPanel() {
		basketStackPanel = new StackLayoutPanel(Unit.PX);
		basketStackPanel.setPixelSize(ClientConstants.SHOPBASKET_STACKPANEL_WIDTH, ClientConstants.SHOPBASKET_STACKPANEL_HEIGHT);
		basketStackPanel.setStyleName("shopBasketStackPanel");
		basketStackPanelContainer.add(basketStackPanel);
	}
	
	/*
	 * TEST METHOD
	 * TODO: Delete this
	 */
	public void getShoppedBasketResults(String basketID) {
		BASKETS = new ArrayList<ShoppedBasket>();
		
		basketService.shopUserBasket(basketID, new AsyncCallback<ShoppingResults>() {
			
			public void onFailure(Throwable caught) {
				clientFactory.getLogger().severe("Error shopping basket: " + caught.getLocalizedMessage());
				Window.alert("MapSelectedBasketView.populateStackPanel - " + caught.getMessage());
			}

			public void onSuccess(ShoppingResults result) {			
				if(result.baskets != null){
					BASKETS = result.baskets;
				}else{
					BASKETS = new ArrayList<ShoppedBasket>();
				}
				updateStackPanelUI();
				updateMapForStore();
			}
		});
	}
	
	private void updateStackPanelUI(){
		
		basketStackPanel.clear();
	 	
		for(ShoppedBasket b: BASKETS){
			basketStackPanel.add(createStackPanelItem(b), createStackPanelHeader(b.store.storeName), ClientConstants.SHOPBASKET_STACKPANEL_HEADERHEIGHT);
		}
	}

	@UiHandler("backButton")
	void onClickBack(ClickEvent e)
	{
		clientFactory.getPlaceController().goTo(new AddUserBasketPlace());
	}

	private void initMap(){
		//mapContainerPanel.add(map);
	}
	
	private void updateMapForStore(){ 
		/*		
	    try {
			MapOptions options = new MapOptions();
			// Zoom level. Required
			options.setZoom(8);
			// Open a map centered on Cawker City, KS USA. Required
			options.setCenter(new LatLng(49.25, -123.14));
			// Map type. Required.
			options.setMapTypeId(new MapTypeId().getRoadmap());
			
			// Enable maps drag feature. Disabled by default.
			options.setDraggable(true);
			// Enable and add default navigation control. Disabled by default.
			options.setNavigationControl(true);
			// Enable and add map type control. Disabled by default.
			options.setMapTypeControl(true);
			map = new MapWidget(options);
			mapContainerPanel.add(map);
		} catch (Exception e) {
			mapContainerPanel.add(new Label("Map Goes Here"));
			e.printStackTrace();
		}
		map.clearOverlays();
	    LatLng storeLoc = LatLng.newInstance(store.location.latitude, store.location.longitude);
	    map.setCenter(storeLoc);
	    map.setZoomLevel(10);

	    map.setSize("100%", "100%");
	    // Add some controls for the zoom level
	    map.addControl(new LargeMapControl());

	    // Add a marker
	    map.addOverlay(new Marker(storeLoc));

	    // Add an info window to highlight a point of interest
	    map.getInfoWindow().open(map.getCenter(), new InfoWindowContent(store.storeName));*/
	}
	
/*	private void updateMapForShoppedBasket(){ 
		
		map.clearOverlays();
	    map.setZoomLevel(10);

	    map.setSize("100%", "100%");
	    // Add some controls for the zoom level
	    map.addControl(new LargeMapControl());

	    // Add a marker
	    for(ShoppedBasket s: BASKETS){
	    	//map.addOverlay(new Marker(LatLng.newInstance(s.store.location.latitude, s.store.location.longitude)));
	    	Random r = new Random();
	    	Double lat = r.nextDouble() * new Double("0.001") + new Double("49.25");
    		Double lng = r.nextDouble() * new Double("0.001") + new Double("-123.14");
	    	map.addOverlay(new Marker(LatLng.newInstance(lat, lng)));
	    }
	    
	    map.getInfoWindow().open(map.getCenter(), new InfoWindowContent("Hi Piers"));
	    // Add an info window to highlight a point of interest
	   // map.getInfoWindow().open(map.getCenter(), new InfoWindowContent(store.storeName));
	}*/
		
	private Widget createStackPanelItem(ShoppedBasket basket) {
		    // Create the list of items
		    VerticalPanel storePanel = new VerticalPanel();
		    //storePanel.setSpacing(4);
		    storePanel.setPixelSize(ClientConstants.SHOPBASKET_STACKPANEL_WIDTH, ClientConstants.SHOPBASKET_STACKPANEL_HEIGHT - (ClientConstants.SHOPBASKET_STACKPANEL_HEADERHEIGHT * BASKETS.size()));
		    storePanel.setStyleName("shopBasketStackPanelItem");
		    
		    for (BasketItem b: basket.items) {
		      storePanel.add(new Label(b.description + " " + b.price.toString()));
		    }
		    
		    storePanel.add(new Label("Total: " + basket.totalPrice.toString()));
		    return new SimplePanel(storePanel);
	}
	
	  private Widget createStackPanelHeader(String text) {
		    // Add the image and text to a horizontal panel
		    HorizontalPanel hPanel = new HorizontalPanel();
		    hPanel.setPixelSize(ClientConstants.SHOPBASKET_STACKPANEL_WIDTH, ClientConstants.SHOPBASKET_STACKPANEL_HEADERHEIGHT);
		    hPanel.setWidth("100%");
		   // hPanel.setSpacing(0);
		    hPanel.setVerticalAlignment(HasVerticalAlignment.ALIGN_MIDDLE);
		    HTML headerText = new HTML(text);
		    hPanel.setStyleName("shopBasketStackPanelHeader");
		    hPanel.add(headerText);
		    return new SimplePanel(hPanel);
		  }
	
	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
}
