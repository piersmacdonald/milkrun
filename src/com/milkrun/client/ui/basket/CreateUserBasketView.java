package com.milkrun.client.ui.basket;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestionEvent;
import com.google.gwt.user.client.ui.SuggestionHandler;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.basket.ShopSelectedBasketPlace;
import com.milkrun.client.services.UserBasketService;
import com.milkrun.client.services.UserBasketServiceAsync;
import com.milkrun.client.ui.components.ItemMultiWordSuggestion;
import com.milkrun.client.ui.components.ItemSuggestOracle;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.UserBasket;


/**
 * @author Piers
 *
 */
public class CreateUserBasketView extends Composite
{
	private static AddUserBasketViewUiBinder uiBinder = GWT.create(AddUserBasketViewUiBinder.class);
	interface AddUserBasketViewUiBinder extends UiBinder<Widget, CreateUserBasketView>{}
	
	private final UserBasketServiceAsync basketService = GWT.create(UserBasketService.class);
	
	@UiField(provided = true) CellTable<BasketItem> itemTable;
	@UiField Button addButton;
	@UiField Button mapBasketButton;
	@UiField(provided = true) SuggestBox selectItemTextBox;	
	
	private ArrayList<BasketItem> ITEMS;
	private ItemSuggestOracle itemOracle;
	private BasketItem selectedItem;
	private ClientFactory clientFactory;
	
	private UserBasket userBasket;

	public CreateUserBasketView()
	{	
		//Create the UI table
		ITEMS = new ArrayList<BasketItem>();
		createBasketTable();
		
		//Create the custom Oracle to predict user item an
		// Tie the oracl to the suggest box and set up event handling.
		itemOracle = new ItemSuggestOracle(); 
		selectItemTextBox = new SuggestBox(itemOracle);

		/*
		 * Event handler to pick up the suggestion event when a user selects an item
		 * assigns the selectedItem property incase we want to add that to the basket 
		 */
		selectItemTextBox.addEventHandler(new SuggestionHandler(){
			@Override
			public void onSuggestionSelected(SuggestionEvent event) {
				updateSelectedItem(event);
			}
			
		});
	    
			
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	@UiHandler("mapBasketButton")
	void onClickMap(ClickEvent e)
	{
		userBasket = new UserBasket();
		userBasket.items = ITEMS;
		userBasket.userID = "piers";
		userBasket.description = "Random Test Basket";
		userBasket.createUserId = "p";
		userBasket.lastUpdateUserId = "p";
		
		basketService.createUserBasket(userBasket, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				 //userBasket.userBasketID = Integer.parseInt(result);
				 //clientFactory.setUserBasket(userBasket);
				 clientFactory.getPlaceController().goTo(new ShopSelectedBasketPlace(result));
			}
			
			@Override
			public void onFailure(Throwable caught) {
				System.out.println(caught.getStackTrace().toString());
				clientFactory.getLogger().severe("Error creating basket");
			}
		});
	}
	
	
	

	@UiHandler("addButton")
	void onClickAdd(ClickEvent e)
	{
		updateTable(selectedItem);
	}
	
	private void updateSelectedItem(SuggestionEvent e){
		ItemMultiWordSuggestion sItem = (ItemMultiWordSuggestion) e.getSelectedSuggestion();	
		BasketItem bItem = sItem.getSuggestionItem();
		selectedItem = bItem;
	}
	
	private void updateTable(BasketItem item) {

		ITEMS.add(item);
		// Set the total row count
		itemTable.setRowCount(ITEMS.size(), true);
		// Push the data into the widget.
		itemTable.setRowData(0, ITEMS);
	}

	private void createBasketTable() {
		itemTable = new CellTable<BasketItem>();
		itemTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

		// Create name column
		TextColumn<BasketItem> idColumn = new TextColumn<BasketItem>() {
			@Override
			public String getValue(BasketItem item) {
				return item.basketItemID + "";
			}
		};
		itemTable.addColumn(idColumn, "Item ID");
		
		// Create name column
		TextColumn<BasketItem> descColumn = new TextColumn<BasketItem>() {
			@Override
			public String getValue(BasketItem item) {
				return item.description;
			}
		};
		itemTable.addColumn(descColumn, "Description");
		
		// Create address column
		TextColumn<BasketItem> brandColumn = new TextColumn<BasketItem>() {
			@Override
			public String getValue(BasketItem item) {
				if(item.brand == null)
					return "";
				else	
					return item.brand.brandName;
			}
		};		
		itemTable.addColumn(brandColumn, "Brand");

		
		// Add a selection model to handle user selection.
		final SingleSelectionModel<BasketItem> selectionModel = new SingleSelectionModel<BasketItem>();
		itemTable.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					@Override
					public void onSelectionChange(SelectionChangeEvent event) {
						BasketItem selected = selectionModel.getSelectedObject();
						if (selected != null) {
							//clientFactory.setSelectedContactId(selected.getId().toString());
						}
					}
				});
	}


	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
}
