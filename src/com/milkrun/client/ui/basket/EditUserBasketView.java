package com.milkrun.client.ui.basket;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;


public class EditUserBasketView extends Composite
{

	private static AddBasketItemViewUiBinder uiBinder = GWT.create(AddBasketItemViewUiBinder.class);
	interface AddBasketItemViewUiBinder extends UiBinder<Widget, EditUserBasketView>{}
	
	@UiField TextBox descriptionTxt;
	@UiField TextBox brandTxt;
	@UiField Button addButton;
	
	private ClientFactory clientFactory;

	public EditUserBasketView()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("addButton")
	void onClickAdd(ClickEvent e)
	{
/*		BasketItemRequest request = clientFactory.getRequestFactory().itemRequest();
		BasketItemResourceProxy newItem = request.create(BasketItemResourceProxy.class);

		newItem.setBrandName(brandTxt.getText());
		newItem.setItemDescription(descriptionTxt.getText());
		
		
		request.persist().using(newItem);
		request.fire(new Receiver<Void>()
		{
			@Override
		  public void onSuccess(Void arg0)
		    {
			  clientFactory.getPlaceController().goTo(new ListBasketItemPlace());
		    }
		});
		*/
		
/*        // Save contact
        editableContact.setName(txtName.getText());
        editableContact.setAddress(txtAddress.getText());
        request.persist().using(editableContact);
        request.fire(
                        new Receiver<Void>() {
                                @Override
                                public void onSuccess(Void response) {
                                        // Move to ViewContactsPlace
                                        clientFactory.getPlaceController().goTo(new ViewContactsPlace());
                                }
                        }
        );
*/
	}
	

	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
	
}
