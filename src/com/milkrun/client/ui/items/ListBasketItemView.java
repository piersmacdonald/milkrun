package com.milkrun.client.ui.items;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.milkrun.client.services.BasketItemService;
import com.milkrun.client.services.BasketItemServiceAsync;
import com.milkrun.shared.dto.BasketItem;


public class ListBasketItemView extends Composite 
{
	private static ListBasketItemViewUiBinder uiBinder = GWT.create(ListBasketItemViewUiBinder.class);	
	interface ListBasketItemViewUiBinder extends UiBinder<Widget, ListBasketItemView>	{}
	private final BasketItemServiceAsync itemService = GWT.create(BasketItemService.class);
	
	
    private List<BasketItem> ITEMS;

    @UiField(provided = true) CellTable<BasketItem> itemsTable;
	
	public ListBasketItemView()
	{
		//Create the UI table
		ITEMS = new ArrayList<BasketItem>();
		createTable();
		
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void fillTable() {

		try{
			itemService.listBasketItems(new AsyncCallback<List<BasketItem>>() {
				
				@Override
				public void onSuccess(List<BasketItem> result) {
	            	ITEMS = result;
	            	updateTable();
				}

				@Override
				public void onFailure(Throwable caught) {
	            	caught.printStackTrace();
	            	Window.alert("fail");
				}
			});
				
		}catch(Exception e){
			Window.alert(e.getLocalizedMessage());
		}

	}
	
	private void updateTable() {
		// Set the total row count
		itemsTable.setRowCount(ITEMS.size(), true);
		// Push the data into the widget.
		itemsTable.setRowData(0, ITEMS);
	}
	
	private void createTable() {
		itemsTable = new CellTable<BasketItem>();
		itemsTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);

		// Create name column
		TextColumn<BasketItem> descColumn = new TextColumn<BasketItem>() {
			@Override
			public String getValue(BasketItem item) {
				return item.description;
			}
		};
		
		itemsTable.addColumn(descColumn, "Description");

/*	    // Associate an async data provider to the table
		 // XXX: Use AsyncCallback in the method onRangeChanged
		 // to  get the data from the server side
		    AsyncDataProvider<BasketItem> provider = new AsyncDataProvider<BasketItem>() {
		      @Override
		      protected void onRangeChanged(HasData<BasketItem> display) {
		        int start = display.getVisibleRange().getStart();
		        int end = start + display.getVisibleRange().getLength();
		        end = end >= ITEMS.size() ? ITEMS.size() : end;
		        List<BasketItem> sub = ITEMS.subList(start, end);
		        updateRowData(start, sub);
		      }
		    };
		    
		    provider.addDataDisplay(itemsTable);
		    provider.updateRowCount(ITEMS.size(), true);*/
		    
		// Add a selection model to handle user selection.
		final SingleSelectionModel<BasketItem> selectionModel = new SingleSelectionModel<BasketItem>();
		itemsTable.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					@Override
					public void onSelectionChange(SelectionChangeEvent event) {
						BasketItem selected = selectionModel.getSelectedObject();
						if (selected != null) {
							//clientFactory.setSelectedContactId(selected.getId().toString());
						}
					}
				});
	}      
}
