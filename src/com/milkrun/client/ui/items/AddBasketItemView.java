package com.milkrun.client.ui.items;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.item.ListBasketItemPlace;
import com.milkrun.client.services.BasketItemService;
import com.milkrun.client.services.BasketItemServiceAsync;
import com.milkrun.shared.dto.BasketItem;

public class AddBasketItemView extends Composite
{

	private static AddBasketItemViewUiBinder uiBinder = GWT.create(AddBasketItemViewUiBinder.class);
	private final BasketItemServiceAsync itemService = GWT.create(BasketItemService.class);
	interface AddBasketItemViewUiBinder extends UiBinder<Widget, AddBasketItemView>{}
	
	@UiField TextBox descriptionTxt;
	@UiField TextBox itemNameTxt;
	@UiField TextBox brandTxt;
	
	@UiField TextBox upcTxt;
	@UiField ListBox upcTypeListBox;
	
	@UiField TextBox quantityTxt;
	@UiField ListBox unitOfMeasurementList;
	
	
	
	@UiField Button addButton;
	
	private ClientFactory clientFactory;
	
	public AddBasketItemView()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("addButton")
	void onClickAdd(ClickEvent e)
	{
		BasketItem newItem = new BasketItem();
		newItem.description = descriptionTxt.getText();
		newItem.brand.brandName = brandTxt.getText();
		newItem.upc.upiCode = upcTxt.getText(); //todo, create UPI object
		
		itemService.createBasketItem(newItem, new AsyncCallback<String>(){
			@Override
			public void onFailure(Throwable caught) {
				Window.alert(caught.getLocalizedMessage());
			}

			@Override
			public void onSuccess(String result) {
				clientFactory.getPlaceController().goTo(new ListBasketItemPlace());
			}
		});
	}
	

	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
	
	public void populateUI(){
		populateUpcType();
	}
	
	private void populateUpcType(){
		itemService.getUpcType(new AsyncCallback<List<String>>(){

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(List<String> result) {
				for(String s: result){
					upcTypeListBox.addItem(s);
				}
			}});
	}
	
}
