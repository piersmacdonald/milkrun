package com.milkrun.client.ui.items;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;



public class UploadBasketItemsView extends Composite
{

	private static UploadBasketItemsViewUiBinder uiBinder = GWT.create(UploadBasketItemsViewUiBinder.class);
	interface UploadBasketItemsViewUiBinder extends UiBinder<Widget, UploadBasketItemsView>{}
	
	@UiField FileUpload filePathTxt;	
	@UiField Button sendButton;
	
	private ClientFactory clientFactory;
	
	
	public UploadBasketItemsView()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("sendButton")
	void onClickAdd(ClickEvent e)
	{
	}
	

	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
	
}
