package com.milkrun.client.ui.items;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.milkrun.client.services.BasketItemService;
import com.milkrun.client.services.BasketItemServiceAsync;
import com.milkrun.client.utilities.ClientConstants;
import com.milkrun.shared.dto.BasketItem;

public class EditBasketItemView extends Composite
{
	private static EditItemViewImplUiBinder uiBinder = GWT.create(EditItemViewImplUiBinder.class);
	interface EditItemViewImplUiBinder extends UiBinder<Widget, EditBasketItemView>	{}
	private final BasketItemServiceAsync itemService = GWT.create(BasketItemService.class);
	
	@UiField(provided = true) CellTable<BasketItem> itemsTable;
	private List<BasketItem> ITEMS;
	
	@UiField(provided = true)
	SimplePager pager;
	
	@UiField TextBox descriptionTxt;
	@UiField TextBox brandTxt;
	@UiField TextBox idTxt;
	@UiField Button saveButton;
	


	public EditBasketItemView()
	{
		createTable();
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("saveButton")
	void onClickSave(ClickEvent e)
	{
	}

	public void fillTable() {
		try{
			itemService.listBasketItems(new AsyncCallback<List<BasketItem>>() {
				
				@Override
				public void onSuccess(List<BasketItem> result) {
	            	ITEMS = result;
	            	updateTable();
				}

				@Override
				public void onFailure(Throwable caught) {
	            	caught.printStackTrace();
	            	Window.alert("fail");
				}
			});
				
		}catch(Exception e){
			Window.alert(e.getLocalizedMessage());
		}
	}
	
	private void updateTable() {
		// Set the total row count
		itemsTable.setRowCount(ITEMS.size(), true);
		// Push the data into the widget.
		itemsTable.setRowData(0, ITEMS);
	}
	
	private void createTable() {
		itemsTable = new CellTable<BasketItem>(ClientConstants.ITEMS_TABLE_MAX_ROWS);
		itemsTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		
		SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
		pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
	    pager.setDisplay(itemsTable);

		// Create name column
		TextColumn<BasketItem> descColumn = new TextColumn<BasketItem>() {
			@Override
			public String getValue(BasketItem item) {
				return item.description;
			}
		};
		
		itemsTable.addColumn(descColumn, "Description");

		// Add a selection model to handle user selection.
		final SingleSelectionModel<BasketItem> selectionModel = new SingleSelectionModel<BasketItem>();
		itemsTable.setSelectionModel(selectionModel);
		
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					@Override
					public void onSelectionChange(SelectionChangeEvent event) {
						BasketItem selected = selectionModel.getSelectedObject();
						if (selected != null) {
							//clientFactory.setSelectedContactId(selected.getId().toString());
						}
					}
				});
		
	} 
		
}
