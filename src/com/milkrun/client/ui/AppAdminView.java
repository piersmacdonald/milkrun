package com.milkrun.client.ui;


import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;



public class AppAdminView extends Composite
{

	private static AppAdminViewUiBinder uiBinder = GWT.create(AppAdminViewUiBinder.class);
	interface AppAdminViewUiBinder extends UiBinder<Widget, AppAdminView>{}
	
	@UiField Label addUserLink;
	@UiField Label editPermissionsLink;
	@UiField Label statsLink;
		
	public AppAdminView()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}

}
