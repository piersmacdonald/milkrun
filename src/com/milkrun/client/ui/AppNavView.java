package com.milkrun.client.ui;


import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.item.AddBasketItemPlace;
import com.milkrun.client.place.item.ListBasketItemPlace;
import com.milkrun.client.utilities.LanguageUtil;



public class AppNavView extends Composite
{

	private static AppAdminViewUiBinder uiBinder = GWT.create(AppAdminViewUiBinder.class);
	interface AppAdminViewUiBinder extends UiBinder<Widget, AppNavView>{}
	
	@UiField Hyperlink addItemLink;
	@UiField Hyperlink viewItemLink;

	private ClientFactory clientFactory;
	
	
	@UiHandler("addItemLink")
	void onClickLink(ClickEvent e)
	{
		clientFactory.getPlaceController().goTo(new AddBasketItemPlace());
	}
	
	@UiHandler("viewItemLink")
	void onClickViewLink(ClickEvent e)
	{
		clientFactory.getPlaceController().goTo(new ListBasketItemPlace());
	}
		
	public AppNavView()
	{
		//doLanguage();
		initWidget(uiBinder.createAndBindUi(this));
	}



	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
	
	private void doLanguage(){
		addItemLink = new Hyperlink(LanguageUtil.getPhrase("addItemViewLinkEN", "EN"), "AddBasketItemPlace");
	}
}
