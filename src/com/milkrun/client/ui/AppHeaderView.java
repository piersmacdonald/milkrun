package com.milkrun.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.AppNavPlace;
import com.milkrun.client.place.basket.AddUserBasketPlace;

public class AppHeaderView extends Composite
{
	private static AppHeaderViewUiBinder uiBinder = GWT.create(AppHeaderViewUiBinder.class);
	interface AppHeaderViewUiBinder extends UiBinder<Widget, AppHeaderView>{}


	@UiField Button shopLinkButton;
	@UiField Button communityLinkButton;
	@UiField Button adminLinkButton;
	private ClientFactory clientFactory;

	public AppHeaderView()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}

    public void setClientFactory(ClientFactory cf) {
        clientFactory = cf;
    }
    
	@UiHandler("shopLinkButton")
	void onCreateBasketButtonClick(ClickEvent event) {
		clientFactory.getPlaceController().goTo(new AddUserBasketPlace());
	}
	
	@UiHandler("communityLinkButton")
	void onCommunityButtonClick(ClickEvent event) {
		//clientFactory.getPlaceController().goTo(new AddBasketItemPlace());
		clientFactory.getPlaceController().goTo(new AppNavPlace("community"));
	}
	
	
	@UiHandler("adminLinkButton")
	void onListBasketButtonClick(ClickEvent event) {
		if(clientFactory != null) {
			clientFactory.getPlaceController().goTo(new AppNavPlace("admin"));
		}
	}

/*	@UiHandler("addItemButton")
	void onAddItemButtonClick(ClickEvent event) {
		clientFactory.getPlaceController().goTo(new AddBasketItemPlace());
	}
	
	@UiHandler("listItemButton")
	void onListItemButtonClick(ClickEvent event) {
		if(clientFactory != null) {
			clientFactory.getPlaceController().goTo(new ListBasketItemPlace());
		}
	}
	

	}*/
	


}
