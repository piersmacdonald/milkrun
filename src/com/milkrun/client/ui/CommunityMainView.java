package com.milkrun.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.item.AddBasketItemPlace;
import com.milkrun.client.place.item.EditBasketItemPlace;
import com.milkrun.client.place.item.ListBasketItemPlace;
import com.milkrun.client.place.store.StoreItemMapPlace;
import com.milkrun.client.place.store.StorePlace;
import com.milkrun.client.utilities.ClientConstants;


/**
 * @author Piers
 *
 */
public class CommunityMainView extends Composite
{
	private static CommunityMainViewUiBinder uiBinder = GWT.create(CommunityMainViewUiBinder.class);
	interface CommunityMainViewUiBinder extends UiBinder<Widget, CommunityMainView>{}
	
	@UiField Button addItemLink;
	@UiField Button viewItemLink;
	@UiField Button editItemLink;
	
	@UiField Button addStoreLink;
	@UiField Button editStoreLink;
	@UiField Button manageItemStoreMapLink;
	
	@UiField Label linkDescriptionLabel;
	
	private ClientFactory clientFactory;
	
	public CommunityMainView()
	{	
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	/*
	 * Click Handlers
	 */
	@UiHandler("addItemLink")
	void onClickAddItemLink(ClickEvent e)
	{
		clientFactory.getPlaceController().goTo(new AddBasketItemPlace());
	}
	
	@UiHandler("editItemLink")
	void onClickEditItemLink(ClickEvent e)
	{
		clientFactory.getPlaceController().goTo(new EditBasketItemPlace("123"));
	}
	
	
	@UiHandler("viewItemLink")
	void onClickViewLink(ClickEvent e)
	{
		clientFactory.getPlaceController().goTo(new ListBasketItemPlace());
	}

	@UiHandler("addStoreLink")
	void onClickAddStoreLink(ClickEvent e)
	{
		clientFactory.getPlaceController().goTo(new StorePlace(ClientConstants.ACTIVITY_ADD));
	}
	
	@UiHandler("editStoreLink")
	void onClickEditStoreLink(ClickEvent e)
	{
		clientFactory.getPlaceController().goTo(new StorePlace(ClientConstants.ACTIVITY_LIST));
	}
	
	@UiHandler("manageItemStoreMapLink")
	void onClickItemStoreMappingLink(ClickEvent e)
	{
		clientFactory.getPlaceController().goTo(new StoreItemMapPlace());
	}
	
	/*
	 * Button Mouse Over handlers
	 */
	@UiHandler("addItemLink")
	void onHoverAddItemLink(MouseOverEvent e)
	{
		linkDescriptionLabel.setText("Add new shopping items");
		addItemLink.setStyleName("communityButtons[hover]");
	}
	
	@UiHandler("editItemLink")
	void onHoverEditItemLink(MouseOverEvent e)
	{
		linkDescriptionLabel.setText("Edit items");
		editItemLink.setStyleName("communityButtons[hover]");
	}	
	
	@UiHandler("viewItemLink")
	void onHoverViewItemLink(MouseOverEvent e)
	{
		linkDescriptionLabel.setText("View existing items");
		viewItemLink.setStyleName("communityButtons[hover]");
	}
	
	@UiHandler("addStoreLink")
	void onHoverAddStoreLink(MouseOverEvent e)
	{
		linkDescriptionLabel.setText("Add a new store for others to compare prices with");
		addStoreLink.setStyleName("communityButtons[hover]");
	}
	
	@UiHandler("editStoreLink")
	void onHoverEditStoreLink(MouseOverEvent e)
	{
		linkDescriptionLabel.setText("Edit retail store list");
		addStoreLink.setStyleName("communityButtons[hover]");
	}
	
	@UiHandler("manageItemStoreMapLink")
	void onHoverItemStoreLink(MouseOverEvent e)
	{
		linkDescriptionLabel.setText("Enter pricing information for specific items or stores");
		manageItemStoreMapLink.setStyleName("communityButtons[hover]");
	}

	/*
	 * Mouse hover off handlers
	 */
	@UiHandler("addItemLink")
	void onHoverOutAddItemLink(MouseOutEvent e)
	{
		addItemLink.setStyleName("communityButtons");
	}
	
	@UiHandler("editItemLink")
	void onHoverOutEditItemLink(MouseOutEvent e)
	{
		editItemLink.setStyleName("communityButtons");
	}
	
	@UiHandler("viewItemLink")
	void onHoverOutViewItemLink(MouseOutEvent e)
	{
		viewItemLink.setStyleName("communityButtons");
	}
	
	@UiHandler("addStoreLink")
	void onHoverOutAddStoreLink(MouseOutEvent e)
	{
		addStoreLink.setStyleName("communityButtons");
	}
	
	@UiHandler("editStoreLink")
	void onHoverOutEditStoreLink(MouseOutEvent e)
	{
		editStoreLink.setStyleName("communityButtons");
	}
	
	@UiHandler("manageItemStoreMapLink")
	void onHoverOutManageItemStoreMapLink(MouseOutEvent e)
	{
		manageItemStoreMapLink.setStyleName("communityButtons");
	}	
	
	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
}
