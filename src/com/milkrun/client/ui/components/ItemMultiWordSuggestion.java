package com.milkrun.client.ui.components;

import com.google.gwt.user.client.ui.MultiWordSuggestOracle.MultiWordSuggestion;
import com.milkrun.shared.dto.BasketItem;


public class ItemMultiWordSuggestion extends MultiWordSuggestion{
	
    private BasketItem suggestionItem = null;
    
    public ItemMultiWordSuggestion(BasketItem item)
    {
        super(item.description, item.description);
        this.suggestionItem = item;
    }
 
 
    /**
     * @return the SuggestionItem
     */
     public BasketItem getSuggestionItem()
     {
         return suggestionItem;
     }
}
