package com.milkrun.client.ui.components;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.AbstractImagePrototype;

public class FilterableHeader {

	   /**
     * Image resources.
     */
    public static interface Resources extends ClientBundle
    {

    }

    private static final Resources RESOURCES = GWT.create(Resources.class);
    private static final int IMAGE_WIDTH = 16;


    private static String makeImage(ImageResource resource)
    {
        AbstractImagePrototype proto = AbstractImagePrototype.create(resource);
        return proto.getHTML().replace("style='", "style='position:absolute;right:0px;top:0px;");
    }

    private String text;

    public FilterableHeader(String text)
    {
       // super(new ClickableTextCell());
        this.text = text;
    }

    public String getValue()
    {
        return text;
    }

    public void render(Cell.Context context, SafeHtmlBuilder safe)
    {
        int imageWidth = IMAGE_WIDTH;

        StringBuilder sb = new StringBuilder();
        sb.append("<div style='position:relative;cursor:hand;cursor:pointer;");
        sb.append("padding-right:");
        sb.append(imageWidth);
        sb.append("px;'>");
        sb.append("<div>");
        sb.append(text);
        sb.append("</div></div>");

        safe.append(SafeHtmlUtils.fromSafeConstant(sb.toString()));
    }

	
}
