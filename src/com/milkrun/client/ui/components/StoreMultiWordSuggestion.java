package com.milkrun.client.ui.components;

import com.google.gwt.user.client.ui.MultiWordSuggestOracle.MultiWordSuggestion;
import com.milkrun.shared.dto.RetailStore;


public class StoreMultiWordSuggestion extends MultiWordSuggestion{
	
    private RetailStore suggestionStore = null;
    
    public StoreMultiWordSuggestion(RetailStore store)
    {
        super(store.storeName, store.storeName);
        this.suggestionStore = store;
    }
 
 
    /**
     * @return the SuggestionItem
     */
     public RetailStore getSuggestionStore()
     {
         return suggestionStore;
     }
}
