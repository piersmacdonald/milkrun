package com.milkrun.client.ui.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.milkrun.client.services.ItemSuggestionService;
import com.milkrun.client.services.ItemSuggestionServiceAsync;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.ItemSuggestion;


public class ItemSuggestOracle extends SuggestOracle
{
    private List<ItemMultiWordSuggestion> itemSuggestions = null;
    //private final ItemSuggestionResourceProxy itemSuggestProxy = GWT.create(ItemSuggestionResourceProxy.class);
    //private static final AutoBeanFactory autoBeanFactory = GWT.create(AutoBeanFactory.class);
	private final ItemSuggestionServiceAsync itemSuggestService = GWT.create(ItemSuggestionService.class);
    
    @Override
    public void requestSuggestions(Request request, Callback callback)
    {
        Response resp = new Response( matchingItem( request.getQuery(), request.getLimit() ) );

        callback.onSuggestionsReady(request, resp);
    }
 
    /**
     * 
     * @param query The current text being entered into the suggest box
     * @param limit The maximum number of results to return 
     * @return A collection of suggestions that match.
     */
     
     public Collection<ItemMultiWordSuggestion> matchingItem(String query, int limit)
     {
         List<ItemMultiWordSuggestion> matchingItem = new ArrayList<ItemMultiWordSuggestion>(limit);
         
         itemSuggestService.suggestByString(query.trim(), new AsyncCallback<ItemSuggestion>() {
						@Override
						public void onFailure(Throwable caught) {
							System.out.println("help ItemSuggestOracle.suggestByString");
						}

						@Override
						public void onSuccess(ItemSuggestion result) {

						if ( itemSuggestions == null )
						{
							itemSuggestions = new ArrayList<ItemMultiWordSuggestion>();
						}	    
						try{
							itemSuggestions.clear();
							for(BasketItem i : result.suggestedItems){
								itemSuggestions.add(new ItemMultiWordSuggestion(i));
							}
						}catch(Exception e){
						}
		
						}
					});
         
         // only begin to search after the user has type two characters
         if ( query.length() >= 1 && itemSuggestions != null)
         {
             String prefixToMatch = query.toLowerCase();
 
             int i = 0;
             int s = itemSuggestions.size();
 
             // Skip forward over all the names that don't match at the beginning of the array.
             while (i < s && !itemSuggestions.get(i).getDisplayString().toLowerCase().startsWith(prefixToMatch) )
             {
                 i++;
             }
 
             // Now we are at the start of the block of matching names. Add matching names till we
             // run out of names, stop finding matches, or have enough matches.
             int count = 0;
 
             while (i < s && itemSuggestions.get(i).getDisplayString().toLowerCase().startsWith(prefixToMatch) && count < limit) 
             {
                 matchingItem.add( itemSuggestions.get(i) );
                 i++;
                 count++;
             }
 
         }
 
             return matchingItem;
     }
 
 
 
/*        *//**
         * @param o 
         * @return
         * @see java.util.List#add(java.lang.Object)
         *//*
         public boolean add(ItemMultiWordSuggestion o)
         {
             if ( itemSuggestions == null )
             {
                itemSuggestions = new ArrayList<ItemMultiWordSuggestion>();
             }
 
             return itemSuggestions.add(o);
         }
 
         *//**
          * @param o 
          * @return
          * @see java.util.List#add(java.lang.Object)
          *//*
          public boolean add(ItemSuggestion o)
          {
              if ( itemSuggestions == null )
              {
                 itemSuggestions = new ArrayList<ItemMultiWordSuggestion>();
              }
              
              try{
            	  //itemSuggestions.clear();
                  for(BasketItem i : o.getSuggestedItems()){
                	  itemSuggestions.add(new ItemMultiWordSuggestion(i));
                  }
                  return true;
              
              }catch(Exception e){
            	  return false;
              }

          }
  
 
        *//**
         * @param o
         * @return
         * @see java.util.List#remove(java.lang.Object)
         *//*
         public boolean remove(Object o)
         {
             if ( itemSuggestions != null)
             {
                 return itemSuggestions.remove(o);
             }
 
             return false;
          }

         *//**
          * @param o
          * @return
          * @see java.util.List#remove(java.lang.Object)
          *//*
          public void removeAll()
          {
              if ( itemSuggestions != null)
              {
            	  itemSuggestions.clear();
              }
        	  

          }*/

/*         itemSuggestProxy.getClientResource().setReference(sw);

try{
    itemSuggestProxy.suggestByString(new Result<ItemSuggestion>(){
		public void onFailure(Throwable caught) { 
			System.out.println("help ItemSuggestOracle.suggestByString");
		}
	 	public void onSuccess(ItemSuggestion rep) {
			ItemSuggestion suggestion = rep;
			try {
				String s = new JsonRepresentation(rep).getJsonObject().toString();
				suggestion = DTOConversionUtil.ItemSuggestionFromJson(s, autoBeanFactory);
			} catch (IOException e1) {
			// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (Exception e1) {
			// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			 
			if ( itemSuggestions == null )
			{
				itemSuggestions = new ArrayList<ItemMultiWordSuggestion>();
			}	    
			try{
				itemSuggestions.clear();
				for(BasketItem i : suggestion.suggestedItems){
					itemSuggestions.add(new ItemMultiWordSuggestion(i));
				}
			}catch(Exception e){
			}
		}				 
    });*/
         
         
}