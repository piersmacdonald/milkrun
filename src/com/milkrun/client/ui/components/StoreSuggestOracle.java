package com.milkrun.client.ui.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.milkrun.client.services.StoreService;
import com.milkrun.client.services.StoreServiceAsync;
import com.milkrun.shared.dto.RetailStore;


public class StoreSuggestOracle extends SuggestOracle
{
    private List<StoreMultiWordSuggestion> storeSuggestions = null;
    private final StoreServiceAsync storeService = GWT.create(StoreService.class);
    
    @Override
    public void requestSuggestions(Request request, Callback callback)
    {
        Response resp = new Response( matchingItem( request.getQuery(), request.getLimit() ) );

        callback.onSuggestionsReady(request, resp);
    }
 
    /**
     * 
     * @param query The current text being entered into the suggest box
     * @param limit The maximum number of results to return 
     * @return A collection of suggestions that match.
     */
     
     public Collection<StoreMultiWordSuggestion> matchingItem(String query, int limit)
     {
         List<StoreMultiWordSuggestion> matchingItem = new ArrayList<StoreMultiWordSuggestion>(limit);
         
         storeService.suggestStoreByString(query.trim(), new AsyncCallback<List<RetailStore>>() {
						@Override
						public void onFailure(Throwable caught) {
							System.out.println("help ItemSuggestOracle.suggestByString");
						}

						public void onSuccess(List<RetailStore> result) {

						if ( storeSuggestions == null )
						{
							storeSuggestions = new ArrayList<StoreMultiWordSuggestion>();
						}	    
						try{
							storeSuggestions.clear();
							for(RetailStore i : result){
								storeSuggestions.add(new StoreMultiWordSuggestion(i));
							}
						}catch(Exception e){
						}
		
						}
					});
         
         // only begin to search after the user has type two characters
         if ( query.length() >= 1 && storeSuggestions != null)
         {
             String prefixToMatch = query.toLowerCase();
 
             int i = 0;
             int s = storeSuggestions.size();
 
             // Skip forward over all the names that don't match at the beginning of the array.
             while (i < s && !storeSuggestions.get(i).getDisplayString().toLowerCase().startsWith(prefixToMatch) )
             {
                 i++;
             }
 
             // Now we are at the start of the block of matching names. Add matching names till we
             // run out of names, stop finding matches, or have enough matches.
             int count = 0;
 
             while (i < s && storeSuggestions.get(i).getDisplayString().toLowerCase().startsWith(prefixToMatch) && count < limit) 
             {
                 matchingItem.add( storeSuggestions.get(i) );
                 i++;
                 count++;
             }
 
         }
 
             return matchingItem;
     }         
}