package com.milkrun.client.ui.store;

import java.math.BigDecimal;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SuggestBox;
import com.google.gwt.user.client.ui.SuggestionEvent;
import com.google.gwt.user.client.ui.SuggestionHandler;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.ui.components.ItemMultiWordSuggestion;
import com.milkrun.client.ui.components.ItemSuggestOracle;
import com.milkrun.client.ui.components.StoreMultiWordSuggestion;
import com.milkrun.client.ui.components.StoreSuggestOracle;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.RetailStore;


public class StoreItemMapView extends Composite
{

	private static StoreItemMapViewUiBinder uiBinder = GWT.create(StoreItemMapViewUiBinder.class);
	interface StoreItemMapViewUiBinder extends UiBinder<Widget, StoreItemMapView>{}
	
	@UiField(provided = true) SuggestBox itemTextBox;	
	@UiField(provided = true) SuggestBox storeTextBox;	
	@UiField TextBox priceTextBox;
	@UiField Button addMappingButton;
		
	private ItemSuggestOracle itemOracle;
	private StoreSuggestOracle storeOracle;
	
	private RetailStore store;
	private BasketItem item;

	private ClientFactory clientFactory;
	
	public StoreItemMapView()
	{
		//Create the custom Oracle to predict user item an
		// Tie the oracle to the suggest box and set up event handling.
		itemOracle = new ItemSuggestOracle(); 
		storeOracle = new StoreSuggestOracle();
		
		itemTextBox = new SuggestBox(itemOracle);
		storeTextBox = new SuggestBox(storeOracle);
		
		itemTextBox.addEventHandler(new SuggestionHandler(){
			@Override
			public void onSuggestionSelected(SuggestionEvent e) {
				ItemMultiWordSuggestion suggestion = (ItemMultiWordSuggestion) e.getSelectedSuggestion();	
				BasketItem b = suggestion.getSuggestionItem();
				item = b;
			}
			
		});
	    
		
		storeTextBox.addEventHandler(new SuggestionHandler(){
			@Override
			public void onSuggestionSelected(SuggestionEvent e) {
				StoreMultiWordSuggestion suggestion = (StoreMultiWordSuggestion) e.getSelectedSuggestion();	
				RetailStore s = suggestion.getSuggestionStore();
				store = s;
			}
			
		});
	    
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("addMappingButton")
	void onClickAdd(ClickEvent e)
	{
		if(validateAdd()){
			item.price = new BigDecimal(priceTextBox.getText());	
			try {
				clientFactory.getStoreService().mapItemToStore(store, item, new AsyncCallback<String>(){

					@Override
					public void onFailure(Throwable caught) {
						clientFactory.getLogger().severe("StoreItemMapView StoreService.mapItemToStore" + caught.getMessage());
						
					}

					@Override
					public void onSuccess(String result) {
						// TODO Auto-generated method stub
						
					}});
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}else{
			Window.alert("form incomplete");
		}
		

	}

	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
	
	private boolean validateAdd(){
		if(!priceTextBox.getText().isEmpty()){
			return true;
		}else
			return false;
	}
}
