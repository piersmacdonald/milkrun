package com.milkrun.client.ui.store;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractEditableCell;
import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.ClickableTextCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.utilities.ClientConstants;
import com.milkrun.shared.dto.BasketItem;
import com.milkrun.shared.dto.RetailStore;
import com.milkrun.shared.dto.geo.Location;


public class EditStoreView extends Composite
{

	private static EditStoreViewUiBinder uiBinder = GWT.create(EditStoreViewUiBinder.class);
	interface EditStoreViewUiBinder extends UiBinder<Widget, EditStoreView>{}
	
	@UiField(provided = true) CellTable<BasketItem> inventoryTable;
	private List<AbstractEditableCell<?, ?>> editableCells;
	private List<BasketItem> INVENTORY;
	@UiField(provided = true)
	SimplePager pager;
	
	@UiField TextBox storeNameTxt;
	@UiField TextBox storeNumTxt;
	@UiField TextArea addressText;
	
	@UiField Button saveButton;
	
	private RetailStore store;
	private ClientFactory clientFactory;
	
	public EditStoreView()
	{
		INVENTORY = new ArrayList<BasketItem>(); 
		createTable();
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	public void refresh(String id){
		store = new RetailStore();
		clientFactory.getStoreService().getRetailStore(id, new AsyncCallback<RetailStore>(){

			public void onFailure(Throwable caught) {
				Window.alert("EditStoreView.refresh.getRetailStore" + caught.getMessage());				
			}

			public void onSuccess(RetailStore result) {
				store = result;		
				fillTable();
				bindFormToStore();
			}});
	}

	@UiHandler("saveButton")
	void onClickAdd(ClickEvent e)
	{
		bindStoreToForm();
		clientFactory.getStoreService().updateRetailStore(store, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("fail - storeService.updateRetailStore");
				
			}

			@Override
			public void onSuccess(String result) {
							
			}
		});
	}
	
	public void fillTable() {
		try{
			clientFactory.getStoreService().getPricedItems(store, new AsyncCallback<List<BasketItem>>() {
				
				public void onSuccess(List<BasketItem> result) {
					
					INVENTORY = result;
	            	updateTable();
				}
				
				public void onFailure(Throwable caught) {
	            	caught.printStackTrace();
	            	Window.alert("fail - storeService.getPricedItems");
				}
			});
				
		}catch(Exception e){
			Window.alert(e.getLocalizedMessage());
		}
	}
	
	private void updateTable() {
		// Set the total row count
		inventoryTable.setRowCount(INVENTORY.size(), true);
		// Push the data into the widget.
		inventoryTable.setRowData(0, INVENTORY);
	}
	
	private void createTable() {
		try {
			inventoryTable = new CellTable<BasketItem>(ClientConstants.ITEMS_TABLE_MAX_ROWS);
			inventoryTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
			
			SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
			pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
			pager.setDisplay(inventoryTable);

			// Create storename column
			TextColumn<BasketItem> itemDescColumn = new TextColumn<BasketItem>() {
				@Override
				public String getValue(BasketItem item) {
					return item.description;
				}
			};
			
			// Create storename column
			TextColumn<BasketItem> itemPriceColumn = new TextColumn<BasketItem>() {
				@Override
				public String getValue(BasketItem item) {
					return item.price.toString();
				}
			};
			
			// Create storename column
			TextColumn<BasketItem> itemBrandColumn = new TextColumn<BasketItem>() {
				@Override
				public String getValue(BasketItem item) {
					return item.brand.brandName;
				}
			};
			
		    // ClickableTextCell.
		    addColumn(new ClickableTextCell(), "price", new GetValue<String>() {
		    	public String getValue(BasketItem item) {
		        return "Click " + item.price;
		      }
		    }, new FieldUpdater<BasketItem, String>() {
		      @Override
		      public void update(int index, BasketItem item, String value) {
		        Window.alert("You clicked " + item.description);
		      }
		    });
			
			inventoryTable.addColumn(itemBrandColumn, "Brand");
			inventoryTable.addColumn(itemDescColumn, "Description");
			inventoryTable.addColumn(itemPriceColumn, "Price");
			
			// Add a selection model to handle user selection.
			final SingleSelectionModel<BasketItem> selectionModel = new SingleSelectionModel<BasketItem>();
			inventoryTable.setSelectionModel(selectionModel);
			
			//fill the rest of the form
			selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
						@Override
						public void onSelectionChange(SelectionChangeEvent event) {
							BasketItem selectedItem = selectionModel.getSelectedObject();
						}
					});
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void bindFormToStore(){
		storeNameTxt.setText(store.storeName);
		storeNumTxt.setText(store.storeUniqueID);
		addressText.setText(store.location.fullAddress);
	}
	
	private void bindStoreToForm(){
		store.storeName = storeNameTxt.getText();
		store.storeUniqueID = storeNumTxt.getText();
		store.location = new Location();
		store.location.fullAddress = addressText.getText();
	}
	
	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
	
	  /**
	   * Add a column with a header.
	   * 
	   * @param <C> the cell type
	   * @param cell the cell used to render the column
	   * @param headerText the header string
	   * @param getter the value getter for the cell
	   */
	  private <C> Column<BasketItem, C> addColumn(Cell<C> cell, String headerText,
	      final GetValue<C> getter, FieldUpdater<BasketItem, C> fieldUpdater) {
	    Column<BasketItem, C> column = new Column<BasketItem, C>(cell) {
	      @Override
	      public C getValue(BasketItem object) {
	        return getter.getValue(object);
	      }
	    };
	    column.setFieldUpdater(fieldUpdater);
	    if (cell instanceof AbstractEditableCell<?, ?>) {
	      editableCells.add((AbstractEditableCell<?, ?>) cell);
	    }
	    inventoryTable.addColumn(column, headerText);
	    return column;
	  }
	  
	  /**
	   * Get a cell value from a record.
	   * 
	   * @param <C> the cell type
	   */
	  private static interface GetValue<C> {
	    C getValue(BasketItem item);
	  }
}
