package com.milkrun.client.ui.store;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.services.StoreService;
import com.milkrun.client.services.StoreServiceAsync;
import com.milkrun.shared.dto.RetailStore;
import com.milkrun.shared.dto.geo.Location;


public class AddStoreView extends Composite
{

	private static AddBasketItemViewUiBinder uiBinder = GWT.create(AddBasketItemViewUiBinder.class);
	private final StoreServiceAsync storeService = GWT.create(StoreService.class);
	interface AddBasketItemViewUiBinder extends UiBinder<Widget, AddStoreView>{}
	
	@UiField TextBox storeNameTxt;
	@UiField TextBox storeNumTxt;
	@UiField TextArea addressText;
	
	@UiField Button addButton;
	
	private RetailStore store;
	private ClientFactory clientFactory;
	
	public AddStoreView()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("addButton")
	void onClickAdd(ClickEvent e)
	{
		bindFormToStore();
		storeService.createRetailStore(store, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onSuccess(String result) {
				Window.alert("Added Store, new ID:" + result);
			}
		});
	}
		
	private void bindFormToStore(){
		store = new RetailStore();
		store.storeName = storeNameTxt.getText();
		store.storeUniqueID = storeNumTxt.getText();
		store.location = new Location();
		store.location.fullAddress = addressText.getText();
	}
	
	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
}
