package com.milkrun.client.ui.store;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy.KeyboardSelectionPolicy;
import com.google.gwt.user.cellview.client.SimplePager;
import com.google.gwt.user.cellview.client.SimplePager.TextLocation;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.milkrun.client.ClientFactory;
import com.milkrun.client.place.store.EditStorePlace;
import com.milkrun.client.utilities.ClientConstants;
import com.milkrun.shared.dto.RetailStore;


public class ListStoreView extends Composite
{

	private static EditStoreViewUiBinder uiBinder = GWT.create(EditStoreViewUiBinder.class);
	interface EditStoreViewUiBinder extends UiBinder<Widget, ListStoreView>{}
	
	@UiField(provided = true) CellTable<RetailStore> storeTable;
	private List<RetailStore> STORES;
	@UiField(provided = true)
	SimplePager pager;
	
	private ClientFactory clientFactory;
	
	public ListStoreView()
	{
		STORES = new ArrayList<RetailStore>(); 
		createTable();
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void fillTable() {
		try{
			clientFactory.getStoreService().listRetailStores(new AsyncCallback<List<RetailStore>>() {
				
				public void onSuccess(List<RetailStore> result) {
	            	STORES = result;
	            	updateTable();
				}

				public void onFailure(Throwable caught) {
	            	caught.printStackTrace();
	            	Window.alert("fail - storeService.listRetailStores");
				}
			});
				
		}catch(Exception e){
			Window.alert(e.getLocalizedMessage());
		}
	}
	
	private void updateTable() {
		// Set the total row count
		storeTable.setRowCount(STORES.size(), true);
		// Push the data into the widget.
		storeTable.setRowData(0, STORES);
	}
	
	private void createTable() {
		storeTable = new CellTable<RetailStore>(ClientConstants.ITEMS_TABLE_MAX_ROWS);
		storeTable.setKeyboardSelectionPolicy(KeyboardSelectionPolicy.ENABLED);
		
		SimplePager.Resources pagerResources = GWT.create(SimplePager.Resources.class);
		pager = new SimplePager(TextLocation.CENTER, pagerResources, false, 0, true);
	    pager.setDisplay(storeTable);

		// Create storename column
		TextColumn<RetailStore> storeNameColumn = new TextColumn<RetailStore>() {
			@Override
			public String getValue(RetailStore store) {
				return store.storeName;
			}
		};
		
		// Create storename column
		TextColumn<RetailStore> storeNumColumn = new TextColumn<RetailStore>() {
			@Override
			public String getValue(RetailStore store) {
				return store.storeUniqueID;
			}
		};
		
		// Create storename column
		TextColumn<RetailStore> locationColumn = new TextColumn<RetailStore>() {
			@Override
			public String getValue(RetailStore store) {
				return store.location.fullAddress;
			}
		};
		
		storeTable.addColumn(storeNameColumn, "Name");
		storeTable.addColumn(storeNumColumn, "Number");
		storeTable.addColumn(locationColumn, "Location");
		
		// Add a selection model to handle user selection.
		final SingleSelectionModel<RetailStore> selectionModel = new SingleSelectionModel<RetailStore>();
		storeTable.setSelectionModel(selectionModel);
		
		//fill the rest of the form
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
					@Override
					public void onSelectionChange(SelectionChangeEvent event) {
						RetailStore selectedStore = selectionModel.getSelectedObject();
						if (selectedStore != null) {
							clientFactory.getPlaceController().goTo(new EditStorePlace(selectedStore.retailStoreID+""));
						}
					}
				});
		
	}
		
	public void setClientFactory(ClientFactory cf) {
		clientFactory = cf;
	}
}
