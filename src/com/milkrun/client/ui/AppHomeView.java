package com.milkrun.client.ui;


import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;



public class AppHomeView extends Composite
{

	private static AppHomeViewUiBinder uiBinder = GWT.create(AppHomeViewUiBinder.class);
	interface AppHomeViewUiBinder extends UiBinder<Widget, AppHomeView>{}
	
	@UiField Label welcomeText;

		
	public AppHomeView()
	{
		initWidget(uiBinder.createAndBindUi(this));
		welcomeText.setText("Welcome to Milk Run. To start use the navigation bar at the top to begin shopping, contribute to our data collection or manage admin settings");

	}
	
	public void setWelcomeMessage(){
	}

}
