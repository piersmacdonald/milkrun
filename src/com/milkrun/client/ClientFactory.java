package com.milkrun.client;

import java.util.logging.Logger;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.milkrun.client.services.StoreServiceAsync;
import com.milkrun.client.ui.AppAdminView;
import com.milkrun.client.ui.AppHeaderView;
import com.milkrun.client.ui.AppHomeView;
import com.milkrun.client.ui.AppNavView;
import com.milkrun.client.ui.CommunityMainView;
import com.milkrun.client.ui.basket.CreateUserBasketView;
import com.milkrun.client.ui.basket.EditUserBasketView;
import com.milkrun.client.ui.basket.ListUserBasketView;
import com.milkrun.client.ui.basket.ShopSelectedBasketView;
import com.milkrun.client.ui.items.AddBasketItemView;
import com.milkrun.client.ui.items.EditBasketItemView;
import com.milkrun.client.ui.items.ListBasketItemView;
import com.milkrun.client.ui.store.AddStoreView;
import com.milkrun.client.ui.store.EditStoreView;
import com.milkrun.client.ui.store.ListStoreView;
import com.milkrun.client.ui.store.StoreItemMapView;
import com.milkrun.shared.dto.UserBasket;

public interface ClientFactory
{
	EventBus getEventBus();
	PlaceController getPlaceController();
	
	/*
	 * User related
	 */
	
	//User getUser();
	UserBasket getUserBasket();
	void setUserBasket(UserBasket basket);
	
	/*
	 * Views
	 */
	EditBasketItemView getEditBasketItemView();
	AddBasketItemView getAddBasketItemView();
	ListBasketItemView getListBasketItemView();
	
	EditUserBasketView getEditUserBasketView();
	CreateUserBasketView getAddUserBasketView();
	ListUserBasketView getListUserBasketView();
	ShopSelectedBasketView getMapSelectedBasketView();
	
	StoreItemMapView getStoreItemMapView();
	AddStoreView getAddStoreView();
	EditStoreView getEditStoreView();
	ListStoreView getListStoreView();
	
	AppAdminView getAppAdminView();
	AppHeaderView getAppHeaderView();
	AppNavView getAppNavView();
	AppHomeView getAppHomeView();
	
	CommunityMainView getCommunityMainView();
	
	/*
	 * Other utils
	 */
	Logger getLogger();
	
	StoreServiceAsync getStoreService();
	
}
