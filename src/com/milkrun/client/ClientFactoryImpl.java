package com.milkrun.client;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;
import com.milkrun.client.services.StoreService;
import com.milkrun.client.services.StoreServiceAsync;
import com.milkrun.client.ui.AppAdminView;
import com.milkrun.client.ui.AppHeaderView;
import com.milkrun.client.ui.AppHomeView;
import com.milkrun.client.ui.AppNavView;
import com.milkrun.client.ui.CommunityMainView;
import com.milkrun.client.ui.basket.CreateUserBasketView;
import com.milkrun.client.ui.basket.EditUserBasketView;
import com.milkrun.client.ui.basket.ListUserBasketView;
import com.milkrun.client.ui.basket.ShopSelectedBasketView;
import com.milkrun.client.ui.items.AddBasketItemView;
import com.milkrun.client.ui.items.EditBasketItemView;
import com.milkrun.client.ui.items.ListBasketItemView;
import com.milkrun.client.ui.store.AddStoreView;
import com.milkrun.client.ui.store.EditStoreView;
import com.milkrun.client.ui.store.ListStoreView;
import com.milkrun.client.ui.store.StoreItemMapView;
import com.milkrun.shared.dto.UserBasket;

public class ClientFactoryImpl implements ClientFactory
{
	//Factorys, buses, automobiles
	private static final EventBus eventBus = new SimpleEventBus();
	private static final PlaceController placeController = new PlaceController(eventBus);
	private static final Logger log = Logger.getLogger(ClientFactoryImpl.class.getName());

	//Views
	private static final EditBasketItemView editBasketItemView = new EditBasketItemView();
	private static final AddBasketItemView addBasketItemView = new AddBasketItemView();
	private static final ListBasketItemView listBasketItemView	= new ListBasketItemView();
	
	private static final EditUserBasketView editUserBasketView = new EditUserBasketView();
	private static final CreateUserBasketView addUserBasketView = new CreateUserBasketView();
	private static final ListUserBasketView listUserBasketView	= new ListUserBasketView();	
	private static final ShopSelectedBasketView mapSelectedBasketView = new ShopSelectedBasketView();
	
	private static final StoreItemMapView storeItemMapView = new StoreItemMapView();
	private static final AddStoreView addStoreView = new AddStoreView();
	private static final EditStoreView editStoreView = new EditStoreView();
	private static final ListStoreView listStoreView = new ListStoreView();
	
	private static final AppAdminView appAdminView = new AppAdminView();
	private static final AppHeaderView appHeaderView = new AppHeaderView();
	private static final AppNavView appNavView	= new AppNavView();
	private static final AppHomeView appHomeView	= new AppHomeView();
	
	private static final CommunityMainView commNavView	= new CommunityMainView();
	
	//Services
	private final StoreServiceAsync storeService = GWT.create(StoreService.class);
	
	//User stuff
	private UserBasket userBasket = new UserBasket();


	@Override
	public EventBus getEventBus()
	{
		return eventBus;
	}

	@Override
	public PlaceController getPlaceController()
	{
		return placeController;
	}


	public AddStoreView getAddStoreView()
	{
		return addStoreView;
	}
	
	public EditStoreView getEditStoreView()
	{
		return editStoreView;
	}
	
	
	@Override
	public Logger getLogger() 
	{
		return log;
	}

	@Override
	public AppHeaderView getAppHeaderView() {
		return appHeaderView;
	}

	@Override
	public AddBasketItemView getAddBasketItemView()
	{
		return addBasketItemView;
	}
	
	@Override
	public EditBasketItemView getEditBasketItemView() {
		return editBasketItemView;
	}
	
	@Override
	public ListBasketItemView getListBasketItemView() {
		return listBasketItemView;
	}
	
	@Override
	public ShopSelectedBasketView getMapSelectedBasketView() {
		return mapSelectedBasketView;
	}

	@Override
	public EditUserBasketView getEditUserBasketView() {
		return editUserBasketView;
	}

	@Override
	public CreateUserBasketView getAddUserBasketView() {
		return addUserBasketView;
	}

	@Override
	public ListUserBasketView getListUserBasketView() {
		return listUserBasketView;
	}

	@Override
	public AppAdminView getAppAdminView() {
		return appAdminView;
	}

	@Override
	public UserBasket getUserBasket() {
		return userBasket;
	}
	

	public void setUserBasket(UserBasket basket) {
		userBasket = basket;
	}
	
	public StoreItemMapView getStoreItemMapView(){
		return storeItemMapView;
	}

	public AppNavView getAppNavView() {
		return appNavView;
	}
	
	public AppHomeView getAppHomeView() {
		return appHomeView;
	}


	public CommunityMainView getCommunityMainView() {
		return commNavView;
	}
	
	public StoreServiceAsync getStoreService(){
		return storeService;
	}

	public ListStoreView getListStoreView() {
		return listStoreView;
	}
}
