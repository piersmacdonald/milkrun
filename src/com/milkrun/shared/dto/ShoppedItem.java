package com.milkrun.shared.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ShoppedItem implements IsSerializable {
	
	public int basketItemID;
   
	public String upc;
	public String description;
	public String brandName;

	public BigDecimal price;
	
	public String createUserId;
	public Timestamp createTs;
	public String lastUpdateUserId;
	public Timestamp lastUpdateTs;
	public Timestamp effStartTs;
	public Timestamp effStopTs;
	
}
