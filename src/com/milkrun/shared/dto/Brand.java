package com.milkrun.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Brand implements IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2811138113093351103L;

	public int brandID;
	
	public String brandName;
	public String description;
	public Manufacturer manufacturer;
}
