package com.milkrun.shared.dto;

import java.sql.Timestamp;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class UserBasket implements IsSerializable{

	
	public int userBasketID;
	public String description;
	public String userID;
	
	public List<BasketItem> items;
	
	public String createUserId;
	public Timestamp createTs;
	public String lastUpdateUserId;
	public Timestamp lastUpdateTs;
	public Timestamp effStartTs;
	public Timestamp effStopTs;

}
