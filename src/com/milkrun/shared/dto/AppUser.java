package com.milkrun.shared.dto;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;

public class AppUser implements IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3903479550483522950L;
	public int userID;
	public String firstName;
	public String lastName;
	
	public Date memberSince;
	
}
