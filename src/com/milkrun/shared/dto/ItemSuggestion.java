package com.milkrun.shared.dto;

import java.sql.Timestamp;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ItemSuggestion implements IsSerializable{
	
	private static final long serialVersionUID = 1L;
	public String inputString;
	public List<BasketItem> suggestedItems;
	
	public String createUserId;
	public Timestamp createTs;
	public String lastUpdateUserId;
	public Timestamp lastUpdateTs;
	public Timestamp effStartTs;
	public Timestamp effStopTs;
	

}
