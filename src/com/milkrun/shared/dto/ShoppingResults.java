package com.milkrun.shared.dto;

import java.sql.Timestamp;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class ShoppingResults implements IsSerializable {
	

	
	public List<ShoppedBasket> baskets;
	public int resultCount;
	
	public String createUserId;
	public Timestamp createTs;
	public String lastUpdateUserId;
	public Timestamp lastUpdateTs;
	public Timestamp effStartTs;
	public Timestamp effStopTs;
}
