package com.milkrun.shared.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

import com.google.gwt.user.client.rpc.IsSerializable;

public class BasketItem implements IsSerializable {

	/**
	 * 
	 */
	public int basketItemID;
   
	public UPI upc;
	
	public String name;
	public String description;
	public String detailDescription;
	public Brand brand;
	
	public String quantity;
	public String quantityType;
	
	public BigDecimal price;
	
	public String createUserId;
	public Timestamp createTs;
	public String lastUpdateUserId;
	public Timestamp lastUpdateTs;
	public Timestamp effStartTs;
	public Timestamp effStopTs;
    
}
