package com.milkrun.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

public class Manufacturer implements IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3903479550483522950L;
	public int manufacturerID;
	public String companyName;
	public int parentCompanyID;
	public int primaryAddress;
	public int secondaryAddress;
}
