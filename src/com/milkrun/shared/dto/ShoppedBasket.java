package com.milkrun.shared.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;


public class ShoppedBasket implements IsSerializable {

	public String basketID;
	
	public RetailStore store;
	public BigDecimal totalPrice;
	
	public ShoppedBasket(){}
	
	public List<BasketItem> items;
	
	public String createUserId;
	public Timestamp createTs;
	public String lastUpdateUserId;
	public Timestamp lastUpdateTs;
	public Timestamp effStartTs;
	public Timestamp effStopTs;
}
