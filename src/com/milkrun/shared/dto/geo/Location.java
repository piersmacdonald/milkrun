package com.milkrun.shared.dto.geo;

import java.io.Serializable;
import java.sql.Timestamp;


public class Location implements Serializable{
	
	public int locationID;
   
	public double latitude;
	public double longitude;
	
	public String streetName;
	public String StreetType;
	public String fullAddress;
	
	public String createUserId;
	public Timestamp createTs;
	public String lastUpdateUserId;
	public Timestamp lastUpdateTs;
	public Timestamp effStartTs;
	public Timestamp effStopTs;
	
	@Override
	public String toString(){
		return streetName + " " + StreetType;
	}
	
}
