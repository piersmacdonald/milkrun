package com.milkrun.shared.dto;

import java.sql.Timestamp;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.milkrun.shared.dto.geo.Location;

public class RetailStore implements IsSerializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -525644250636771589L;

	public int retailStoreID;
   
	public String storeName;
	public String storeUniqueID;
	public Location location;
	
	public String createUserId;
	public Timestamp createTs;
	public String lastUpdateUserId;
	public Timestamp lastUpdateTs;
	public Timestamp effStartTs;
	public Timestamp effStopTs;
	
}
