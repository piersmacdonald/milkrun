package com.milkrun.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

public class UPI implements IsSerializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public int upiID;
	public String upiType;
	public String upiCode;
	
}
